/**
 * Small util to create a folder with template files
 */
const program = require('commander');
const inquirer = require('inquirer');
const mkdirp = require('mkdirp');
const insertLine = require('insert-line');
const path = require('path');
const fs = require('fs');
const os = require('os');

const wizard = [
  {
    type: 'input',
    name: 'name',
    message: 'What is the name of the component? (in CamelCase)',
  },
  {
    type: 'input',
    name: 'description',
    message: 'Write a short description that describes the component:',
  },
];

const styledCompTemplate = name =>
  `import styled, { css } from 'styled-components';${os.EOL}${os.EOL}// TODO: fix style for component${
    os.EOL
  }export const ${name}SC = styled.div;${os.EOL}`;

const componentTemplate = name =>
  `// @flow${os.EOL}import * as React from 'react';${os.EOL}import { ${name}SC } from './styled';${os.EOL}${
    os.EOL
  }export const ${name} = () => <${name}SC>{/* Put stuff here.. */}</${name}SC>;${os.EOL}${
    os.EOL
  }// TODO: add defaultProps${os.EOL}${name}.defaultProps = {};${os.EOL}`;

const componentIndexTemplate = name => `// @flow${os.EOL}export { ${name} } from './${name}';${os.EOL}`;

const markDownTemplate = (name, description) => `# ${name}${os.EOL} ${description}`;

const testTemplate = name =>
  `import * as React from 'react';${os.EOL}import renderer from 'react-test-renderer';${
    os.EOL
  }import 'jest-styled-components';${os.EOL}import { ${name} } from './${name}';${
    os.EOL
  }import TILE from '../../../index';${os.EOL}${os.EOL}it('Should render a default ${name} component', () => {${
    os.EOL
  }  const { theme } = TILE.uiStore;${os.EOL}  const component = <${name} theme={theme} />;${
    os.EOL
  }  const tree = renderer.create(component).toJSON();${os.EOL}  expect(tree).toMatchSnapshot();${os.EOL}});${os.EOL}`;

const addFiles = config => {
  const file = [
    {
      extension: '.js',
      name: config.name,
      content: componentTemplate(config.name),
    },
    {
      extension: '.md',
      name: config.name,
      content: markDownTemplate(config.name, config.description),
    },
    {
      extension: '.test.js',
      name: config.name,
      content: testTemplate(config.name),
    },
    {
      extension: '.js',
      name: 'styled',
      content: styledCompTemplate(config.name),
    },
    {
      extension: '.js',
      name: 'index',
      content: componentIndexTemplate(config.name),
    },
  ];

  // add each type of file
  file.forEach(obj => {
    fs.writeFile(`${config.dir}/${obj.name}${obj.extension}`, obj.content, err => {
      if (err) throw err;
    });
  });

  const indexDir = path.join(process.cwd(), `src/index.js`);

  // add reference in index so that it can be used
  insertLine(indexDir)
    .content(`export { ${config.name} } from './components/elements/${config.name}';`)
    .at(50)
    .then(err => {
      if (err) throw err;
    });

  console.log('Done adding component, ', config.name);
};

const addFolder = config => {
  const { name } = config;
  console.log('adding template with name:', name);
  const dir = path.join(process.cwd(), `src/components/elements/${name}`);

  mkdirp(dir, err => {
    if (err) console.error('An error happened, please try again', err);
    else {
      addFiles({ ...config, dir });
    }
  });
};

const addNewComponent = () => {
  inquirer.prompt(wizard).then(answers => {
    inquirer
      .prompt([
        {
          type: 'confirm',
          name: 'confirm',
          message: `You have entered the name: "${answers.name}" and the description: "${
            answers.description
          }". Do you want to proceed?`,
        },
      ])
      .then(answer => {
        const { confirm } = answer;

        if (confirm) {
          addFolder(answers);
        } else {
          addNewComponent();
        }
      });
  });
};

module.exports = {
  addNewComponent,
};
