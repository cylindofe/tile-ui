# TILE

## Getting started

Getting started using TILE:

1.  Clone this repo `git clone git@bitbucket.org:cylindofe/tile-ui.git`
2.  run `npm i` to install all dependencies.
3.  Link tile-ui repo with npm `npm link`. You might have to run the command with `sudo`.
4.  Link the tile-ui to an external project by first starting the node process on tile-ui with `npm start` followed by running `npm link tile-ui` in the external project's root.
5.  You are all set. Start your external project and start using the components `import { Button } from 'tile-ui';`

## Running StoryBook

TILE ships with a build in playground called StoryBook. To start it simply run `npm run storybook` and open a browser window with the generated url.

## Creating a new component

TILE ships with a simple CLI to make it easy to do otherwise cumbersome tasks, such as creating a new component with all component files and dependencies. Using the CLI you can do that in no time:

1.  Run `npm run tile` to start the CLI
2.  Select `Add new component` and follow the steps.
3.  You should now have find a Component folder with the name you assigned in the `src/components/elements` folder.

## Running tests

All components in TILE are unit-tested. to run tests all you have to do is run `npm run test`.

##Type checking

TILE ships with FlowType and every component should have the `// @flow` annotation in the top of every .js file. Check the library for errors using `npm run flow`. You can also check the coverage by using the flow CLI.

##Bugs or Requests?

Please use the Jira board to report bugs or request new features, components etc. [Link to Jira](https://cylin-do.atlassian.net/secure/RapidBoard.jspa?rapidView=10&projectKey=TILE)
