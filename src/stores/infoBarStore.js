// @flow
import { observable, action } from 'mobx';
import { uniqueId, extend, filter } from 'lodash-es';

type InfoBarProps = {
  text: string,
  actions: [],
};

class InfoBarStore {
  @observable activeInfoBars = [];

  @action
  showInfoBar = (props: InfoBarProps) => {
    const id = uniqueId('infobar_');
    const { text, actions } = props;

    const infoBarProps = extend(props, {
      id,
      onRequestHide: () => this.hideInfoBar(id),
      text,
      actions,
    });

    const infoBar = {
      ...infoBarProps,
    };

    this.activeInfoBars.unshift(infoBar);
  };

  @action
  hideInfoBar = async (identifier: string) => {
    this.activeInfoBars = filter(this.activeInfoBars.slice(), obj => obj.id !== identifier);
  };

  @action
  hideAllInfoBars = () => {
    this.activeInfoBars = [];
  };
}

export default InfoBarStore;
