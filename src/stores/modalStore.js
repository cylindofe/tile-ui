// @flow
import { observable, action, computed } from 'mobx';
import { uniqueId, extend, filter } from 'lodash-es';
import ConfirmModal from '../components/containers/ModalContainer/ConfirmModal';

export type ModalProps = {
  title: string,
  closeOnBackdropClick?: boolean,
  confirmHide?: boolean,
  width?: number,
};

export class ModalStore {
  @observable activeModals = [];

  @action
  showModal = (Component: any, props: ModalProps) =>
    new Promise((res, rej) => {
      if (!Component) {
        console.log('[ModalStore: showModal()] No component was passed. Aborting');
        rej();
      }

      const onHide = async (id, data) => {
        await this.hideModal(id);
        res(data);
      };

      const _uniqueId = uniqueId('modal_');

      const { title, width, closeOnBackdropClick, confirmHide, ...rest } = props;

      const modalProps = extend(
        {},
        {
          width: width || 500,
          title,
          id: _uniqueId,
          onRequestHide: data => onHide(_uniqueId, data),
          closeOnBackdropClick: closeOnBackdropClick !== false,
          confirmHide: confirmHide || false,
          passProps: { ...rest },
        }
      );

      const modal = {
        props: {
          ...modalProps,
        },
        Component,
      };

      this.activeModals.push(modal);
    });

  @action
  hideModal = async (identifier: string) => {
    try {
      this.activeModals = filter(this.activeModals.slice(), modal => modal.props.id !== identifier);
    } catch (error) {
      return false;
    }
    return true;
  };

  @action
  confirm = async (text: string) => {
    const props = {
      title: 'Confirm',
      closeOnBackdropClick: false,
      width: 400,
      text,
    };

    return this.showModal(ConfirmModal, props);
  };

  @action
  hideAllModals = () => {
    this.activeModals = [];
  };

  @computed
  get hasModals() {
    return this.activeModals.length > 0;
  }
}

export default ModalStore;
