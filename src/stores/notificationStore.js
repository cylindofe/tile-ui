// @flow
import { observable, action, computed } from 'mobx';
import { uniqueId, extend, filter } from 'lodash-es';

type NotificationType = string;

type NotificationProps = {
  title: string,
  type: NotificationType,
  description?: string,
  delay?: number,
};

class NotificationStore {
  @observable activeNotifications = [];

  @action
  showNotification = (props: NotificationProps) => {
    const getNotificationIcon = (input: NotificationType) => {
      switch (input) {
        case 'success':
          return 'emoticon';
        case 'error':
          return 'error';
        case 'info':
          return 'info';
        default:
          console.log('[NotificationStore: getNotificationIcon] No type matched');
          return null;
      }
    };

    const id = uniqueId('notification_');
    const type = props.type || 'success';
    const icon = getNotificationIcon(type);
    const title = props.title || type;
    const description = props.description || '';
    const delay = props.delay || 3000;

    const notificationProps = extend(props, {
      id,
      onRequestHide: () => this.hideNotifcation(id),
      type,
      icon,
      title,
      description,
      delay,
    });

    const notification = {
      props: {
        ...notificationProps,
      },
    };

    this.activeNotifications.unshift(notification);
  };

  @action
  hideNotifcation = async (identifier: string) => {
    this.activeNotifications = filter(
      this.activeNotifications.slice(),
      notification => notification.props.id !== identifier
    );
  };

  @action
  hideAllNotifications = () => {
    this.activeNotifications = [];
  };
}

export default NotificationStore;
