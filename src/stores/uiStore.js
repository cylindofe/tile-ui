// @flow
import { action, observable, computed } from 'mobx';
import { utils, defaultTheme } from '../theme';
import TILE, { lodash as _ } from '../index';

export class UIStore {
  theme: any;
  _onWindowResize: Function;
  @observable themeInstance: any = null;
  @observable loading: boolean = false;
  @observable breakpointMinWidth: any = null;
  @observable windowInnerWidth: number | null = null;
  @observable windowInnerHeight: number | null = null;

  @computed
  get isInfoBarVisible() {
    return TILE.infoBarStore.activeInfoBars.length > 0;
  }

  constructor() {
    this._onWindowResize = _.debounce(this.setAppSizes, 500);
  }

  @action
  setTheme = (theme: any, force: boolean) => {
    if (this.theme && !force) {
      console.log('[UIStore: setTheme()]: theme already instantiated');
      return null;
    }

    this.removeThemeEventListeners();

    /* If no theme is provided, use default theme */
    const newTheme = utils.buildTheme(theme || defaultTheme);
    this.theme = newTheme;
    this.themeInstance = _.uniqueId('theme_');
    this.addThemeEventListeners();
    this.setAppSizes();
  };

  @action
  addThemeEventListeners = () => {
    window.addEventListener('resize', this._onWindowResize);
  };

  @action
  removeThemeEventListeners = () => {
    window.removeEventListener('resize', this._onWindowResize);
  };

  @action
  setAppSizes = (e: any) => {
    this.windowInnerWidth = window.innerWidth;
    this.windowInnerHeight = window.innerHeight;

    _.forEach(this.theme.breakpoint, (bp, i) => {
      if (this.windowInnerWidth > this.theme.breakpoint[i] && this.windowInnerWidth < this.theme.breakpoint[i + 1]) {
        this.breakpointMinWidth = i;
      }
    });

    if (!this.breakpointMinWidth) this.breakpointMinWidth = 0;
  };

  @action
  setLoading = (value: boolean) => {
    this.loading = value;
  };
}

export default UIStore;
