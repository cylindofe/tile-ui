/**
 * Component Defaults
 * Make sure to use the same properties as used in the themeValues.
 * The values are corresponding to the index of the value of a given property.
 * E.g. fontWeight, borderRadius, etc.
 */
export const componentDefaults = {
  Button: {
    borderRadius: {
      default: 1,
      round: 10,
    },
  },
  Modal: {
    zIndex: {
      default: 0,
    },
  },
  Notification: {
    zIndex: {
      default: 1,
    },
  },
  Loading: {
    zIndex: {
      default: 2,
    },
  },
  P: {
    fontWeight: {
      default: 1,
      thin: 0,
      normal: 1,
      bold: 2,
    },
    color: {
      default: 'text',
      muted: 'icon',
    },
    marginBottom: {
      default: 4,
    },
    fontSize: {
      default: 2,
      small: 1,
      xsmall: 0,
    },
  },
  PanelContainer: {
    color: {
      background: 'secondaryBackground',
    },
    padding: {
      default: 2,
    },
  },
  H1: {
    color: {
      default: 'text',
      muted: 'icon',
      white: 'white',
    },
    marginBottom: {
      default: 4,
    },
    fontSize: {
      default: 9,
    },
    fontWeight: {
      default: 3,
      normal: 1,
      thin: 0,
      bold: 3,
    },
    lineHeight: {
      default: 6,
      multiLine: 13,
    },
    textAlign: {
      default: 'left',
      center: 'center',
    },
  },
  H2: {
    color: {
      default: 'text',
      muted: 'icon',
      white: 'white',
    },
    marginBottom: {
      default: 3,
    },
    fontSize: {
      default: 5,
    },
    fontWeight: {
      default: 3,
      normal: 1,
      thin: 0,
      bold: 3,
    },
    lineHeight: {
      default: 5,
      multiLine: 8,
    },
    textAlign: {
      default: 'left',
      center: 'center',
    },
  },
  H3: {
    color: {
      default: 'text',
      muted: 'icon',
      white: 'white',
    },
    marginBottom: {
      default: 3,
    },
    fontSize: {
      default: 4,
    },
    fontWeight: {
      default: 3,
      normal: 1,
      thin: 0,
      bold: 3,
    },
    lineHeight: {
      default: 4,
      multiLine: 6,
    },
    textAlign: {
      default: 'left',
      center: 'center',
    },
  },
  Link: {
    color: {},
  },
  Separator: {
    color: {
      default: 'separator',
    },
  },
};
