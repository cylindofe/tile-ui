// @flow

/**
 * THEME VALUES
 * Name after camelCased css-in-js properties if possible.
 * E.g. fontWeight, borderRadius, etc.
 *
 */

export const breakpoint: number[] = [0, 600, 900, 1200, 1800];

export const uiSize: number[] = [
  0,
  4,
  8,
  12,
  16,
  20,
  24,
  28,
  32,
  36,
  40,
  44,
  48,
  52,
  56,
  60,
  64,
  68,
  72,
  76,
  80,
  84,
  88,
  92,
  96,
  100,
  104,
  108,
  112,
  116,
  120,
  124,
  128,
  132,
  136,
  140,
  144,
  148,
  152,
  156,
  160,
  164,
];

// export const uiSize: number[] = [20, 24, 32, 40, 64, 128];

export const margin = uiSize;
export const marginBottom = uiSize;
export const marginRight = uiSize;
export const marginLeft = uiSize;
export const marginTop = uiSize;

export const padding = uiSize;
export const paddingBottom = uiSize;
export const paddingRight = uiSize;
export const paddingLeft = uiSize;
export const paddingTop = uiSize;
export const borderRadius = uiSize;

export const width = uiSize;
export const height = uiSize;

export const fontSize: number[] = [8, 10, 12, 14, 16, 20, 24, 28, 32, 36, 40, 44, 48, 64, 72, 96];
export const fontWeight = [100, 400, 500, 700];
export const lineHeight = uiSize;
export const zIndex = [100, 200, 300, 400];

export const textAlign = {
  center: 'center',
  left: 'left',
  right: 'right',
};

export const elevation = [
  'none',
  '0 0 0 1px rgba(48, 72, 87, 0.1)',
  '0 2px 4px 0 rgba(48, 72, 87, 0.1)',
  '0 4px 6px 0 rgba(48, 72, 87, 0.1)',
  '0 5px 15px 0 rgba(0, 0, 0, 0.05), 0 15px 35px 0 rgba(48, 72, 87, 0.1)',
  '0 5px 15px 0 rgba(0, 0, 0, 0.01), 0 5px 35px 0 rgba(48, 72, 87, 0.15), 0 50px 100px 0 rgba(48, 72, 87, 0.1)',
];

export const transitions = ['200ms ease-in-out', '500ms ease-in-out'];

/**
 * Colors
 */
const greys = ['#314756', '#6A7B86', '#87959E', '#BDC5C9', '#D6DBDE', '#EFF1F2', '#F6F7F7', '#FFFFFF'];

const text = greys[0];
const label = greys[1];
const grey = greys[1];
const icon = greys[2];
const muted = greys[3];
const border = greys[4];
const separator = greys[5];
const primaryBackground = greys[6];
const secondaryBackground = greys[7];

const primary = '#49C7BA';
const success = '#3CCF8E';
const danger = '#E25554';
const warning = '#FFD23F';
const info = '#6672E5';
const secondary = '#67D4F8';

export const color = {
  primary,
  success,
  danger,
  warning,
  info,
  secondary,
  text,
  grey,
  label,
  icon,
  muted,
  border,
  separator,
  greys,
  primaryBackground,
  secondaryBackground,
};

const backgroundColor = { ...color };

export const themeValues = {
  breakpoint,
  uiSize,
  margin,
  marginBottom,
  marginLeft,
  marginRight,
  marginTop,
  padding,
  paddingBottom,
  paddingTop,
  paddingLeft,
  paddingRight,
  borderRadius,
  fontSize,
  lineHeight,
  fontWeight,
  zIndex,
  elevation,
  transitions,
  textAlign,
  color,
  backgroundColor,
  width,
  height,
};
