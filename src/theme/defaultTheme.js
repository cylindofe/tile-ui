import { componentDefaults } from './componentDefaults';
import { themeValues } from './themeValues';

export const defaultTheme = {
  componentDefaults,
  themeValues,
};
