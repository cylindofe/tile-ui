// @flow
import { css } from 'styled-components';
import { lodash as _ } from '../index';

/**
 * Returns component values tree.
 * @param {object} componentDefaults : object of component defaults.
 * @param {object} themeValues     : object of theme values.
 */
const buildComponentValues = (componentDefaults, themeValues) => {
  if (!componentDefaults || !themeValues) return null;

  const buildNode = (i, parent) => {
    const node = {};
    Object.keys(i).forEach(property => {
      if (_.isObject(i[property])) {
        node[property] = buildNode(i[property], property);
      } else {
        node[property] = themeValues[parent][i[property]];
      }
    });
    return node;
  };

  return buildNode(componentDefaults, null);
};

/**
 * Returns a complete theme
 * @param {object} componentValues : object of component defaults.
 * @param {object} themeValues     : object of theme values.
 */
export const buildTheme = (theme: any) => ({
  ...theme.themeValues,
  component: buildComponentValues(theme.componentDefaults, theme.themeValues),
});

/**
 * Apply space to a component
 * @param {*} properties what properties to apply, eg. padding-left
 * @param {*} props
 */
export const spaceMixin = (props: any, config: any) => {
  const { uiSize } = props.theme;

  const allow = property => {
    if (!config) return true;
    return config[property] !== false;
  };

  const hasValue = value => {
    if (value !== null && value !== '' && typeof value !== 'undefined') return true;
    return false;
  };

  const output = `${allow('padding') && hasValue(props.padding) ? `padding: ${uiSize[props.padding]}px;` : ``}${
    allow('paddingLeft') && hasValue(props.paddingLeft) ? `padding-left: ${uiSize[props.paddingLeft]}px;` : ``
  }${allow('paddingRight') && hasValue(props.paddingRight) ? `padding-right: ${uiSize[props.paddingRight]}px;` : ``}${
    allow('paddingTop') && hasValue(props.paddingTop) ? `padding-top: ${uiSize[props.paddingTop]}px;` : ``
  } ${
    allow('paddingBottom') && hasValue(props.paddingBottom) ? `padding-bottom: ${uiSize[props.paddingBottom]}px;` : ``
  }${allow('margin') && hasValue(props.margin) ? `margin: ${uiSize[props.margin]}px;` : ``}${
    allow('marginLeft') && hasValue(props.marginLeft) ? `margin-left: ${uiSize[props.marginLeft]}px;` : ``
  }${allow('marginRight') && hasValue(props.marginRight) ? `margin-right: ${uiSize[props.marginRight]}px;` : ``}${
    allow('marginTop') && hasValue(props.marginTop) ? `margin-top: ${uiSize[props.marginTop]}px;` : ``
  }${allow('marginBottom') && hasValue(props.marginBottom) ? `margin-bottom: ${uiSize[props.marginBottom]}px;` : ``}
`;

  return output;
};

/**
 * truncate content of a component
 * @param {*} width
 */
export const truncateMixin = (width: number) => `
    width: ${width}px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;

export const utils = {
  buildTheme,
};
