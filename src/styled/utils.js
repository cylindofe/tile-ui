import TILE, { lodash as _ } from '../index';
import {
  properties,
  spacingPropertiesArray,
  sizePropertiesArray,
  sizeProperties,
  fontProperties,
  fontPropertiesArray,
  positionPropertiesArray,
  flexProperties,
  flexPropertiesArray,
} from './properties';
import { logFromMethod } from '../utils';

//  utils
export const isDefined = n => n !== undefined && n !== null;
export const isNumber = n => _.isNumber(n);
export const isPercent = n => isNumber(n) && n <= 1;
export const isString = s => typeof s === 'string' || s instanceof String;
export const isNegative = n => isNumber(n) && n < 0;
export const isArray = n => Array.isArray(n);

export const toCssProperty = property => {
  if (properties[property]) return properties[property];
  logFromMethod({
    method: 'toCssProperty',
    type: 'warning',
    message: `property "${property}" was not found in constants. Please add it`,
  });

  return property;
};

export const toParentProperty = property => {
  switch (property) {
    case 'marginBottom':
    case 'marginTop':
    case 'marginLeft':
    case 'marginRight':
    case 'margin':
    case 'paddingBottom':
    case 'paddingTop':
    case 'paddingLeft':
    case 'paddingRight':
    case 'padding':
      return 'space';
    case 'height':
    case 'width':
    case 'maxWidth':
    case 'maxHeight':
      return 'size';
    case 'lineHeight':
    case 'textTransform':
    case 'fontStyle':
    case 'textAlign':
      return 'font';
    case 'backgroundColor':
    case 'color':
      return 'color';
    case 'position':
    case 'top':
    case 'bottom':
    case 'left':
    case 'right':
      return 'position';
    default:
      return property;
  }
};
export const toArray = n => (Array.isArray(n) ? n : [n]);
export const toPx = n => (isNumber(n) ? `${n}px` : n);
export const toMediaQuery = n => `@media screen and (min-width: ${toPx(n)})`;
export const toCssPropertyString = (property, value) => {
  const prop = toCssProperty(property);
  return isDefined(prop) && isDefined(value) ? `${toCssProperty(property)}: ${value};` : '';
};

// getters
export const getValue = (props, property, value) => {
  const val = value || props[property];
  const prop = toParentProperty(property);

  if (!isDefined(val) || !prop) return null;

  const getSizeValueFromProperty = (v, exclude, p = 'uiSize') => {
    // if string just return value
    if (exclude !== 'isString' && isString(v)) return v;
    // if percent
    if (exclude !== 'isPercent' && isPercent(v)) return `${100 * v}%`;
    // if number
    if (exclude !== 'isNumber' && isNumber(v)) {
      if (props.theme[p][v]) return toPx(props.theme[p][v]);
      return toPx(v);
    }

    return v;
  };

  const getColorFromProperty = (v, exclude, p = 'color') => {
    if (props.theme[p][v]) return props.theme[p][v];
    return v;
  };

  switch (prop) {
    case 'flex':
      if (isString(val)) return val;
      return isNumber(val) ? val : null;
    case 'size':
      return getSizeValueFromProperty(val);
    case 'space':
      return getSizeValueFromProperty(val, 'isPercent');
    case 'fontSize':
      return getSizeValueFromProperty(val, 'isPercent', 'fontSize');
    case 'font':
      if (isString(val)) return val;
      return getSizeValueFromProperty(val, 'isPercent');
    case 'display':
      return val || 'block';
    case 'background':
    case 'backgroundColor':
    case 'color':
      return getColorFromProperty(val);
    case 'position':
      return getSizeValueFromProperty(val, 'isPercent');
    case 'fontWeight':
      if (isString(val)) return val;
      if (props.theme[prop][val]) return props.theme[prop][val];
      return null;
    case 'flexDirection':
    case 'justifyContent':
    case 'alignItems':
    case 'whiteSpace':
      return isString(val) ? val : null;
    default:
      logFromMethod({
        method: 'getValue',
        type: 'warning',
        message: `property "${property}" was not found. Please add it. (value: ${val})`,
      });

      return getSizeValueFromProperty(val);
  }
};

// Mixins

/**
 * Break point mixin for getting style for a break point
 * @param {*} props
 * @param {*} property
 */
export const breakpointMixin = (props: any, property: string) => {
  const { theme } = props;
  const value = props[property];

  if (isDefined(value)) {
    if (!isArray(value)) {
      return toCssPropertyString(property, getValue(props, property, value));
    }
    const val = _.map(value, (obj, i) => {
      if (!isDefined(obj)) return null;
      return `${toMediaQuery(theme.breakpoint[i])} {
          ${toCssPropertyString(property, getValue(props, property, obj))}
        }`;
    }).join(' ');
    return val;
  }
  return null;
};

/**
 * breakpointMixin() proxy method for setting single property
 * @param {*} props
 * @param {*} property
 */
export const addProperty = (props: any, property: string) => {
  if (!props || !property) return null;

  return breakpointMixin(props, property);
};

/**
 * Apply size to a component
 * @param {*} props
 * @param {*} property
 */
export const sizeMixin = (props, property) => {
  const get = p => {
    const prop = sizeProperties[p];
    if (!prop) return null;

    return breakpointMixin(props, p);
  };

  if (!property) {
    return _.compact(_.map(sizePropertiesArray, p => get(p)));
  }

  return get(property);
};

export const fontMixin = (props, property) => {
  const get = p => {
    const prop = fontProperties[p];
    if (!prop) return null;

    return breakpointMixin(props, p);
  };

  if (!property) {
    return _.compact(_.map(fontPropertiesArray, p => get(p)));
  }

  return get(property);
};

/**
 * Apply space to a component
 * @param {*} props
 * @param {*} config what properties to allow, eg. padding-left: false
 */
export const spaceMixin = (props: any, config: any) => {
  const allow = property => {
    if (!config) return true;
    return config[property] !== false;
  };
  const get = property => allow(property) && breakpointMixin(props, property);

  return _.compact(_.map(spacingPropertiesArray, property => get(property)));
};

/**
 * Apply positioning properties
 * @param {*} props
 * @param {*} config what properties to allow, eg. padding-left: false
 */
export const positionMixin = (props: any, config: any) => {
  const allow = property => {
    if (!config) return true;
    return config[property] !== false;
  };
  const get = property => allow(property) && breakpointMixin(props, property);

  return _.compact(_.map(positionPropertiesArray, property => get(property)));
};

/**
 * Apply positioning properties
 * @param {*} props
 * @param {*} config what properties to allow, eg. padding-left: false
 */
export const flexMixin = (props: any, config: any) => {
  const allow = property => {
    if (!config) return true;
    return config[property] !== false;
  };
  const get = property => allow(property) && breakpointMixin(props, property);

  return _.compact(_.map(flexPropertiesArray, property => get(property)));
};

/**
 * truncate content of a component
 * @param {*} width
 */
export const truncateMixin = (width: number) => `
    width: ${width}px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;
