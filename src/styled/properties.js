export const properties = {
  // margins
  margin: 'margin',
  marginLeft: 'margin-left',
  marginRight: 'margin-right',
  marginTop: 'margin-top',
  marginBottom: 'margin-bottom',
  // paddings
  padding: 'padding',
  paddingLeft: 'padding-left',
  paddingRight: 'padding-right',
  paddingTop: 'padding-top',
  paddingBottom: 'padding-bottom',
  // sizes
  width: 'width',
  height: 'height',
  maxWidth: 'max-width',
  maxHeight: 'max-height',
  // display
  display: 'display',
  // font
  fontSize: 'font-size',
  fontWeight: 'font-weight',
  lineHeight: 'line-height',
  textTransform: 'text-transform',
  fontStyle: 'font-style',
  color: 'color',
  textAlign: 'text-align',
  whiteSpace: 'white-space',
  // position
  left: 'left',
  right: 'right',
  top: 'top',
  bottom: 'bottom',
  position: 'position',
  // flex
  flex: 'flex',
  flexDirection: 'flex-direction',
  flexWrap: 'flex-wrap',
  justifyContent: 'justifyContent',
  alignItems: 'align-items',
  alignContent: 'align-content',
  alignSelf: 'align-self',
  order: 'order',
  flexShrink: 'flex-shrink',
  flexBasis: ' flex-basis',
  // other
  backgroundColor: 'background-color',
};

export const sizeProperties = {
  width: properties.width,
  height: properties.height,
  maxWidth: properties.maxWidth,
  maxHeight: properties.maxHeight,
};

export const fontProperties = {
  fontSize: properties.fontSize,
  fontWeight: properties.fontWeight,
  lineHeight: properties.lineHeight,
  textTransform: properties.textTransform,
  fontStyle: properties.fontStyle,
  color: properties.color,
  textAlign: properties.textAlign,
  whiteSpace: properties.whiteSpace,
};

export const positionProperties = {
  left: properties.left,
  right: properties.right,
  top: properties.top,
  bottom: properties.bottom,
  position: properties.position,
};

export const flexProperties = {
  flex: properties.flex,
  flexDirection: properties.flexDirection,
  flexWrap: properties.flexWrap,
  justifyContent: properties.justifyContent,
  alignItems: properties.alignItems,
  alignContent: properties.alignContent,
  alignSelf: properties.alignSelf,
  order: properties.order,
  flexShrink: properties.flexShrink,
  flexBasis: properties.flexBasis,
};

export const positionPropertiesArray = ['left', 'right', 'top', 'bottom', 'position'];

export const spacingPropertiesArray = [
  'margin',
  'marginLeft',
  'marginRight',
  'marginTop',
  'marginBottom',
  'padding',
  'paddingLeft',
  'paddingRight',
  'paddingTop',
  'paddingBottom',
];

export const sizePropertiesArray = ['width', 'height', 'maxHeight', 'maxWidth'];

export const fontPropertiesArray = [
  'fontSize',
  'fontWeight',
  'lineHeight',
  'textTransform',
  'fontStyle',
  'color',
  'textAlign',
  'whiteSpace',
];

export const flexPropertiesArray = [
  'flex',
  'flexDirection',
  'flexWrap',
  'justifyContent',
  'alignItems',
  'alignContent',
  'alignSelf',
  'order',
  'flexShrink',
  'flexBasis',
];
