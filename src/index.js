// @flow
/**
 * Exports the TILE component library
 */
import { configure } from 'mobx';
import {
  map,
  extend,
  filter,
  get,
  set,
  isArray,
  isObject,
  isNumber,
  forEach,
  groupBy,
  compact,
  find,
  uniqueId,
  isFunction,
  throttle,
  debounce,
} from 'lodash-es';
import T from './TILE';
import * as themeUtils from './theme';
import * as commonUtils from './utils';

/**
 * Component Exports
 */

// Grid
export { Row } from './components/grid/Row';
export { Col } from './components/grid/Col';
export { Container } from './components/grid/Container';
export { Box } from './components/grid/Box';

// Containers
export { AppContainer } from './components/containers/AppContainer';

// Elements
export { Accordion } from './components/elements/Accordion';
export { Button } from './components/elements/Button';
export { Label } from './components/elements/Label';
export { H1 } from './components/elements/H1';
export { H2 } from './components/elements/H2';
export { H3 } from './components/elements/H3';
export { Icon } from './components/elements/Icon';
export { Logo } from './components/elements/Logo';
export { Checkbox } from './components/elements/Checkbox';
export { Tag } from './components/elements/Tag';
export { Link } from './components/elements/Link';
export { Form } from './components/elements/Form';
export { Select } from './components/elements/Select';
export { Input } from './components/elements/Input';

export { Text } from './components/elements/Text';
export { DateTime } from './components/elements/DateTime';
export { Spacer } from './components/elements/Spacer';
export { P } from './components/elements/P';
export { HoverTransition } from './components/elements/HoverTransition';
export { Separator } from './components/elements/Separator';

// Fragments
export { Card } from './components/fragments/Card';
export { FormElement } from './components/fragments/FormElement';
export { SubmitButton } from './components/fragments/SubmitButton';

/**
 * Init methods
 */
configure({ isolateGlobalState: true });

/**
 * Util Export
 */
export const lodash = {
  map,
  extend,
  filter,
  get,
  set,
  isArray,
  isFunction,
  isObject,
  isNumber,
  forEach,
  groupBy,
  compact,
  find,
  uniqueId,
  throttle,
  debounce,
};

/**
 * Tile theme & common utils export
 */
export const theme = {
  ...themeUtils,
};
export const utils = {
  ...commonUtils,
};

const TILE = new T();
export default TILE;
