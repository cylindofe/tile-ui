import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { PanelContainer } from './PanelContainer';
import TILE from '../../../index';

it('Should render a Panel component', () => {
  const { theme } = TILE.uiStore;
  const defaults = theme.component.PanelContainer;

  const component = <PanelContainer render={() => <div>hello</div>} theme={theme}></PanelContainer>;
  const tree = renderer.create(component).toJSON();

  expect(tree).toHaveStyleRule('background', defaults.color.background);
  expect(tree).toMatchSnapshot();
});
