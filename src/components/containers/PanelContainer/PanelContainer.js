// @flow
import * as React from 'react';
import { PanelSC, PanelInnerCS } from './styled';

type Props = {
  isInfoBarVisible: boolean,
  render: any,
  elevation?: number,
  position?: string,
  size?: number,
  zIndex?: number,
};

export const PanelContainer = ({ render, ...rest }: Props) => (
  <PanelSC {...rest}>
    <PanelInnerCS {...rest}>{render()}</PanelInnerCS>
  </PanelSC>
);

PanelContainer.defaultProps = {
  position: 'left',
  elevation: 2,
  size: 10,
  zIndex: 99,
};
