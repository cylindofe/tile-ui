// @flow
/**
 * @file Panel is a wrapper component that is fixed to a window or container side.
 */
export { PanelContainer } from './PanelContainer';
