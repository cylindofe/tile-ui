import styled, { css } from 'styled-components';
import { lodash as _ } from '../../../index';

export const PanelSC = styled.div`
  background: ${props => props.theme.component.PanelContainer.color.background};
  position: fixed;
  overflow: hidden;
  box-sizing: content-box;
  padding: ${props => props.theme.component.PanelContainer.padding.default}px;
  z-index: ${props => props.zIndex};
  transition: all 200ms ease;

  ${props => {
    if (_.isNumber(props.elevation)) {
      return css`
        box-shadow: ${props.theme.elevation[props.elevation]};
      `;
    }
  }};

  ${props => {
    switch (props.position) {
      case 'top':
        return css`
          right: 0;
          left: 0;
          top: 0;
          height: ${props.theme.uiSize[props.size]};
        `;
      case 'right':
        return css`
          right: 0;
          bottom: 0;
          top: 0;
          width: ${props.theme.uiSize[props.size]};
        `;
      case 'left':
        return css`
          left: 0;
          bottom: 0;
          top: 0;
          width: ${props.theme.uiSize[props.size]};
        `;
      default:
        console.log('unknown position');
    }
  }};

  ${props =>
    props.isInfoBarVisible &&
    css`
      top: ${props.theme.uiSize[10]}px;
    `};
`;

export const PanelInnerCS = styled.div`
  box-sizing: content-box;
  height: 100%;
  width: 100%;

  > * {
    vertical-align: top;
  }
`;
