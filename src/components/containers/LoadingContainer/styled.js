import styled, { keyframes, css } from 'styled-components';

const animation = keyframes`
  to {
    transform: rotate(1turn)
  }
`;

export const Loading = styled.div`
  position: absolute;
  width: ${props => props.size}px;
  height: ${props => props.size}px;
  top: 50%;
  left: 50%;
  margin-top: -${props => props.size * 0.5}px;
  margin-left: -${props => props.size * 0.5}px;
  border: ${props => props.size * 0.15}px solid rgba(189, 189, 189, 0.25);
  border-left-color: ${props => props.theme.color.primary};
  border-radius: 50%;
  opacity: 0;
  transition: opacity 200ms ease;
`;

export const LoadingContainerComp = styled.div`
  width: 100vw;
  height: 100vh;
  top: 0;
  left: 0;
  position: fixed;
  background: rgba(255, 255, 255, 0.8);
  opacity: 0;
  transition: opacity 1000ms ease;
  pointer-events: none;
  z-index: ${props => props.theme.component.Loading.zIndex.default};

  ${props =>
    props.loading &&
    css`
      pointer-events: all;
      opacity: 1;

      ${Loading} {
        animation: ${animation} 600ms infinite linear;
        opacity: 0.8;
      }
    `};
`;
