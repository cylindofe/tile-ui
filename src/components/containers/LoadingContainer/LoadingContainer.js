// @flow
import * as React from 'react';
import { type Theme } from 'styled-components';
import { observer } from 'mobx-react';
import TILE from '../../../index';
import { LoadingContainerComp, Loading } from './styled';

type Props = {
  theme?: Theme,
  size?: number,
};

@observer
class LoadingContainer extends React.Component<Props> {
  uiStore: any = TILE.uiStore;
  render() {
    const { size = 80, ...rest } = this.props;

    return (
      <LoadingContainerComp loading={this.uiStore.loading} {...rest}>
        <Loading size={size} {...rest} />
      </LoadingContainerComp>
    );
  }
}

export default LoadingContainer;
