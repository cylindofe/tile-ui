import * as React from 'react';
import { shallow, to, have } from 'enzyme';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import LoadingContainer from './LoadingContainer';
import TILE from '../../../index';

it('Should render a LoadingContainer', () => {
  const { theme } = TILE.uiStore;

  const component = <LoadingContainer theme={theme} />;
  const wrapper = shallow(component);
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
