import * as React from 'react';
import { shallow, to, have } from 'enzyme';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import AppContainer from './AppContainer';

it('Should render an AppContainer with a theme', () => {
  const component = <AppContainer>Hello!</AppContainer>;
  const wrapper = shallow(component);
  const tree = renderer.create(component).toJSON();

  expect(wrapper.props().theme).not.toBe(null);
  expect(wrapper.props().theme).not.toBe({});
  expect(tree).toMatchSnapshot();
});

it('Should render an AppContainer with children', () => {
  const component = (
    <AppContainer>
      <div>I'm a child</div>
    </AppContainer>
  );
  const wrapper = shallow(component);
  const tree = renderer.create(component).toJSON();

  expect(wrapper.find('div').children()).toHaveLength(1);
  expect(tree).toMatchSnapshot();
});
