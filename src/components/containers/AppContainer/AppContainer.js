// @flow
import * as React from 'react';
import { ThemeProvider, injectGlobal } from 'styled-components';
import { observer } from 'mobx-react';
import { AppContainerSC, ChildContainerSC } from './styled';
import TILE from '../../../index';
import { ModalContainer } from '../ModalContainer';
import { NotificationContainer } from '../NotificationContainer';
import { LoadingContainer } from '../LoadingContainer';
import { InfoBarContainer } from '../InfoBarContainer';
import { PanelContainer } from '../PanelContainer';

/**
 * Global resets
 */
injectGlobal`

html,
body,
div,
span,
applet,
object,
iframe,
h1,
h2,
h3,
h4,
h5,
h6,
p,
blockquote,
pre,
a,
abbr,
acronym,
address,
big,
cite,
code,
del,
dfn,
em,
img,
ins,
kbd,
q,
s,
samp,
small,
strike,
strong,
sub,
sup,
tt,
var,
b,
u,
i,
center,
dl,
dt,
dd,
ol,
ul,
li,
fieldset,
form,
label,
legend,
table,
caption,
tbody,
tfoot,
thead,
tr,
th,
td,
article,
aside,
canvas,
details,
embed,
figure,
figcaption,
footer,
header,
hgroup,
menu,
nav,
output,
ruby,
section,
summary,
time,
mark,
audio,
video {
  margin: 0;
  padding: 0;
  border: 0;
  vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
menu,
nav,
section {
  display: block;
}

body {
  line-height: 1;
  font-variant-ligatures: none;
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: antialiased;
  text-decoration-skip-ink: auto;
}

ol,
ul {
  list-style: none;
}

blockquote,
q {
  quotes: none;
}

a {
  text-decoration: none;
}

blockquote:before,
blockquote:after,
q:before,
q:after {
  content: '';
  content: none;
}
table {
  border-collapse: collapse;
  border-spacing: 0;
}

  button {
      cursor: pointer;
      outline: none !important;
  }

  * {
    font-family: 'Montserrat', -apple-system, system-ui, Arial, sans-serif;
    line-height: 1.6;
    box-sizing: border-box;
    margin-top: 0;
  }

`;

type Props = {
  modalContainer: boolean,
  loadingContainer: boolean,
  notificationContainer: boolean,
  bootInternally: boolean,
  rebootOnLoad: boolean,
  infoBar: boolean,
  panel: any,
  theme: {
    componentDefaults: {},
    themeValues: {},
  },
  children: React.Node,
};
@observer
class AppContainer extends React.Component<Props, {}> {
  uiStore: { theme: any, isInfoBarVisible: boolean, themeInstance: string } = TILE.uiStore;

  static defaultProps = {
    bootInternally: true,
    modalContainer: true,
    loadingContainer: true,
    notificationContainer: true,
    infoBar: false,
    rebootOnLoad: false,
    panel: null,
    height: '100vh',
    position: 'fixed',
  };

  componentDidMount() {
    const { children, ...config } = this.props;
    const { bootInternally, rebootOnLoad } = config;

    if ((bootInternally !== false && !TILE.booted) || (bootInternally !== false && rebootOnLoad)) {
      TILE.boot(config);
    }
  }

  render() {
    const {
      children,
      panel,
      infoBar,
      modalContainer,
      loadingContainer,
      notificationContainer,
      theme,
      onScroll,
      height,
      position,
      ...rest
    } = this.props;
    const { isInfoBarVisible } = this.uiStore;

    if (!this.uiStore.themeInstance || !this.uiStore.theme) return null;

    return (
      <ThemeProvider theme={this.uiStore.theme}>
        <React.Fragment>
          {infoBar && <InfoBarContainer />}
          <AppContainerSC height={height} position={position} onScroll={onScroll} isInfoBarVisible={isInfoBarVisible}>
            <ChildContainerSC {...rest}>{children}</ChildContainerSC>
            {panel && <PanelContainer isInfoBarVisible={isInfoBarVisible} {...panel} />}
            {modalContainer && <ModalContainer />}
            {loadingContainer && <LoadingContainer />}
            {notificationContainer && <NotificationContainer />}
          </AppContainerSC>
        </React.Fragment>
      </ThemeProvider>
    );
  }
}

export default AppContainer;
