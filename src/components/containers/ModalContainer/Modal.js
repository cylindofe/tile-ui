// @flow
import React from 'react';
import { observable } from 'mobx';
import { observer } from 'mobx-react';
import styled from 'styled-components';
import { transparentize } from 'polished';
import { Button, H1, H2 } from '../../../index';

type Props = {
  /**
   * Index of the modal
   */
  count: number,
  /**
   * Modal props
   */
  modal: any,
  modalStore: any,
  Component: any,
  onRequestHide: (value: any) => void,
  confirmHide: boolean,
  closeOnBackdropClick: boolean,
  width: number,
  title: string,
  id: string,
};
@observer
export default class Modal extends React.Component<Props> {
  @observable active: boolean = false;

  componentDidMount() {
    setTimeout(() => {
      this.active = true;
    }, 50);
  }

  hide = async () => {
    const { modalStore, onRequestHide, confirmHide } = this.props;

    if (confirmHide) {
      try {
        const res = await modalStore.confirm('Please confirm that you want to close this modal.');
        if (res) onRequestHide(null);
      } catch (error) {
        console.error(error);
        // ...
      }
    } else onRequestHide(null);
  };

  render() {
    const { Component, count, width, title, id, onRequestHide, closeOnBackdropClick, ...rest } = this.props;
    const modalProps = {
      count,
      width,
      title,
      id,
      onRequestHide,
      closeOnBackdropClick,
      ...rest,
    };

    return (
      <ModalBackdrop active={this.active} onClick={closeOnBackdropClick ? this.hide : null}>
        <ModalWrapper active={this.active} width={width} count={count} onClick={e => e.stopPropagation()} key={id}>
          <ModalTitle>
            <H2 marginBottom={0}>{title}</H2>
            <CloseButtonWrapper>
              <Button color="grey" buttonType="flat" shape="round" icon="close" onClick={this.hide} />
            </CloseButtonWrapper>
          </ModalTitle>
          <ModalContent>
            <Component {...modalProps} />
          </ModalContent>
        </ModalWrapper>
      </ModalBackdrop>
    );
  }
}

const ModalBackdrop = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  background: ${props => transparentize(0.5, props.theme.color.greys[1])};
  opacity: ${props => (props.active ? 1 : 0)};
  transition: all 300ms ease;
  overflow: scroll;
`;

const ModalWrapper = styled.div`
  background: #fff;
  border-radius: ${props => props.theme.borderRadius[1]}px;
  box-shadow: ${props => props.theme.elevation[4]};
  max-width: 900px;
  min-height: 250px;
  margin: 0 auto;
  position: relative;
  margin-bottom: 100px;
  margin-top: ${props => props.count * 20 + 100}px;
  opacity: ${props => (props.active ? 1 : 0)};
  transition: all 100ms ease;
`;

const ModalTitle = styled.div`
  padding: ${props => props.theme.uiSize[7]}px;
`;

const CloseButtonWrapper = styled.div`
  position: absolute;
  right: 15px;
  top: 15px;
`;

const ModalContent = styled.div`
  padding: ${props => props.theme.uiSize[8]}px;
`;
