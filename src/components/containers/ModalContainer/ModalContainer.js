import * as React from 'react';
import { observer } from 'mobx-react';
import styled from 'styled-components';
import { map } from 'lodash-es';
import TILE from '../../../index';
import Modal from './Modal';

@observer
class ModalContainer extends React.Component {
  constructor(props) {
    super(props);
    this.modalStore = TILE.modalStore;
  }

  render() {
    const { activeModals } = this.modalStore;

    if (activeModals.length)
      return (
        <ModalContainerSC>
          {map(activeModals, (modal, i) => {
            const { Component, props } = modal;
            return (
              <Modal Component={Component} modalStore={this.modalStore} count={i} key={modal.props.id} {...props} />
            );
          })}
        </ModalContainerSC>
      );
    return null;
  }
}

const ModalContainerSC = styled.div`
  z-index: ${props => props.theme.component.Modal.zIndex.default};
  width: 100vw;
  height: 100vh;
  top: 0;
  left: 0;
  padding: 20px;
  position: fixed;
`;

export default ModalContainer;
