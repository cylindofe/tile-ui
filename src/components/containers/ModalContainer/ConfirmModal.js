// @flow
import * as React from 'react';
import { Button } from '../../../index';

type Props = {
  onRequestHide: (value: any) => void,
};

const ConfirmModal = ({ onRequestHide }: Props) => (
  <React.Fragment>
    <Button buttonType="flat" color="grey" onClick={() => onRequestHide(false)}>
      Cancel
    </Button>
    <Button buttonType="hollow" onClick={() => onRequestHide(true)}>
      Confirm
    </Button>
  </React.Fragment>
);

export default ConfirmModal;
