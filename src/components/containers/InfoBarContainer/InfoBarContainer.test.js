import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { InfoBarContainer } from './InfoBarContainer';
import TILE from '../../../index';

it('Should render a default InfoBarContainer component', () => {
  const { theme } = TILE.uiStore;
  const component = <InfoBarContainer theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
