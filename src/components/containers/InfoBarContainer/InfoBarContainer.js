// @flow
import * as React from 'react';
import { observer } from 'mobx-react';
import { InfoBarContainerSC } from './styled';
import { InfoBar } from './InfoBar';
import TILE from '../../../index';

@observer
class InfoBarContainer extends React.Component<{}, {}> {
  static defaultProps = {};

  store = TILE.infoBarStore;

  render() {
    const { activeInfoBars } = this.store;
    const infoBar = activeInfoBars.length ? activeInfoBars[0] : null;

    return <InfoBarContainerSC>{infoBar && <InfoBar {...infoBar} />}</InfoBarContainerSC>;
  }
}

export { InfoBarContainer };
