// @flow
import * as React from 'react';
import { InfoBarSC } from './styled';

type Props = {
  text: string,
  onRequestHide: () => {},
};

export const InfoBar = ({ text, onRequestHide, ...rest }: Props) => <InfoBarSC onClick={onRequestHide} {...rest}>{text}</InfoBarSC>;
