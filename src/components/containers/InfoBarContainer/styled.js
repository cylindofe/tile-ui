import styled, { css } from 'styled-components';

export const InfoBarContainerSC = styled.div`
  position: fixed;
  width: 100vw;
  top: 0;
  background: ${props => props.theme.color.primary};
  height: ${props => props.theme.uiSize[10]}px;
  z-index: 1;
`;

export const InfoBarSC = styled.div`
  padding: ${props => props.theme.padding[3]}px;
  line-height: ${props => props.theme.lineHeight[4]}px;
  color: white;
`;
