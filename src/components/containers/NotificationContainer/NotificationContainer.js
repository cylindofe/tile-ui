import React, { Component } from 'react';
import styled, { css, withTheme } from 'styled-components';
import { inject, observer } from 'mobx-react';
import { observable, action } from 'mobx';
import { map } from 'lodash-es';
import Notification from './Notification';
import TILE from '../../../index';
// import T from './ui/T';

@observer
class NotificationContainer extends Component {
  constructor(props) {
    super(props);

    this.notificationStore = TILE.notificationStore;
  }

  render() {
    const { activeNotifications } = this.notificationStore;

    if (activeNotifications.length)
      return (
        <NotificationContainerSC>
          <NotificationContainerInner>
            <NotificationList>
              {map(activeNotifications, (notification, i) => (
                <Notification key={notification.props.id} count={i} notification={notification} />
              ))}
            </NotificationList>
          </NotificationContainerInner>
        </NotificationContainerSC>
      );

    return null;
  }
}

const NotificationList = styled.div`
  width: 300px;
  right: 0px;
  top: 0px;
  padding: 20px;
  position: absolute;
`;

const NotificationContainerSC = styled.div`
  width: 100vw;
  height: 100vh;
  top: 0;
  left: 0;
  position: fixed;
  pointer-events: none;
  z-index: ${props => props.theme.component.Notification.zIndex.default};
`;

const NotificationContainerInner = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  padding: 0px;
`;

export default NotificationContainer;
