// @flow
import React, { Component } from 'react';
import styled from 'styled-components';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import { transparentize } from 'polished';
import { P, Label, Icon } from '../../../index';

// import Icon from './ui/Icon';
// import T from './ui/T';

type NotificationType = string;

export type NotificationProps = {
  title: string,
  type: NotificationType,
  description: string,
  delay: number,
  id: string,
  onRequestHide: () => {},
  icon: string,
};

type Props = {
  notification: {
    count: number,
    props: NotificationProps,
  },
};
@observer
class Notification extends Component<Props> {
  @observable active = false;

  componentDidMount() {
    const { props } = this.props.notification;
    const { type = 'success', delay, description } = props;

    setTimeout(() => {
      this.active = true;
    }, 50);

    if (type === 'error') this.log(description);

    setTimeout(this.hide, delay);
  }

  log = (error: string) => {
    console.error('Notification error msg: ', error);
  };

  hide = () => {
    const { props } = this.props.notification;
    const { onRequestHide } = props;

    this.active = false;

    setTimeout(() => {
      onRequestHide();
    }, 500);
  };

  getIconColor = (type: NotificationType) => {
    switch (type) {
      case 'success':
        return 'success';
      case 'error':
        return 'danger';
      case 'info':
        return 'grey';
      default:
        console.log('[Notification: NotificationIcon] No icon found');
    }
  };

  render() {
    const { props, count } = this.props.notification;
    const { type, title, icon, description } = props;

    return (
      <NotificationWrapper active={this.active} count={count} onClick={this.hide}>
        <NotificationIcon type={type}>
          <Icon size={40} color={this.getIconColor(type)} icon={icon} />
        </NotificationIcon>

        <NotificationContent>
          <NotificationTitle>
            <Label>{title}</Label>
          </NotificationTitle>

          <NotificationDescription>
            <P>{description}</P>
          </NotificationDescription>
        </NotificationContent>
      </NotificationWrapper>
    );
  }
}

const NotificationContent = styled.div`
  flex: 100%;
  padding: 10px;
`;

const NotificationIcon = styled.div`
  padding: 10px;
  width: 60px;
`;
const NotificationTitle = styled.div``;
const NotificationDescription = styled.div``;

const NotificationWrapper = styled.div`
  background: ${transparentize(0.1, '#ffffff')};
  border-radius: ${props => props.theme.borderRadius[1]}px;
  box-shadow: ${props => props.theme.elevation[5]};
  min-height: 60px;
  width: 100%;
  overflow: scroll;
  margin-bottom: 10px;
  pointer-events: all;
  display: flex;
  flex-direction: row;
  opacity: ${props => (props.active ? 1 : 0)};
  transition: all 500ms ease;
`;

export default Notification;
