# CONTAINERS

Containers are elements that interacts with the app's global store in some way. They are higher-order "clever" components that will provide functionality either to the app in which they are embedded or the nested child components.
