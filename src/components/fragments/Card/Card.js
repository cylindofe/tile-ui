// @flow
import * as React from 'react';
import { CardSC } from './styled';
import { Box } from '../../../index';

type Props = {
  children: any,
  padding?: 0 | 4 | 8,
};

export const Card = ({ children, padding, ...rest }: Props) => (
  <Box {...rest} borderRadius={2} backgroundColor="white" padding={padding} elevation={4}>
    {children}
  </Box>
);

Card.defaultProps = {
  padding: 4,
};
