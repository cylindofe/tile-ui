import styled, { css } from 'styled-components';
import { spaceMixin, sizeMixin } from '../../../styled/utils';

export const FormElementSC = styled.div`
  width: 100%;
  position: relative;
  margin-bottom: ${props => props.theme.margin[7]}px;

  ${spaceMixin};
`;

export const ErrorWrapper = styled.div`
  position: absolute;
  bottom: -${props => props.theme.uiSize[5]}px;
`;
