// @flow
import * as React from 'react';
import { Text, Box } from '../../../index';
import { ErrorWrapper } from './styled';

type Props = {
  error: string,
  touched: boolean,
};

export const ErrorDisplay = ({ error, touched }: Props) => {
  if (!touched || !error) return null;
  return (
    <ErrorWrapper>
      <Text fontSize={2} color="danger">
        {error}
      </Text>
    </ErrorWrapper>
  );
};
