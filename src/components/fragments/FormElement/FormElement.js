// @flow
import * as React from 'react';
import { FormElementSC } from './styled';
import { Input, Label, Select, Text, Checkbox } from '../../../index';
import { ErrorDisplay } from './ErrorDisplay';

type Props = {
  type: 'input' | 'textarea' | 'select' | 'checkbox' | 'radio',
  onChange: Function,
  onBlur: Function,
  onFocus: Function,
  value: any,
  label: string,
  id: string,
  placeholder: string,
  error: any,
  touched: boolean,
  lines: number,
  options: Array<any>,
  required?: boolean,
};

export const FormElement = ({ type, options, ...rest }: Props) => {
  const { id, label, touched, error, required } = rest;
  const hasError = !!(touched && error);

  const renderElement = props => {
    switch (type) {
      case 'input':
        return <Input {...props} />;
      case 'select':
        return <Select {...props} options={options} />;
      case 'checkbox':
        return <Checkbox {...props} />;
      default:
        return null;
    }
  };

  return (
    <FormElementSC {...rest}>
      <Label htmlFor={id} marginBottom={2}>
        {label}
        {!required && (
          <Text fontSize={2} color="grey" marginLeft={1}>
            (optional)
          </Text>
        )}
      </Label>
      {renderElement({ hasError, ...rest })}
      <ErrorDisplay touched={touched} error={error} />
    </FormElementSC>
  );
};

FormElement.defaultProps = {
  required: true,
};
