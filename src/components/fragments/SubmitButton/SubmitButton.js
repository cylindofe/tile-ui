// @flow
import * as React from 'react';
import { SubmitButtonSC } from './styled';
import { Button } from '../../../index';

type Props = {
  onClick: Function,
  children: any,
};

export const SubmitButton = ({ children, ...rest }: Props) => (
  <Button type="submit" {...rest}>
    {children}
  </Button>
);

SubmitButton.defaultProps = {};
