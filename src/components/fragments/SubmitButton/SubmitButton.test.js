import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { SubmitButton } from './SubmitButton';
import TILE from '../../../index';

it('Should render a default SubmitButton component', () => {
  const { theme } = TILE.uiStore;
  const component = <SubmitButton theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
