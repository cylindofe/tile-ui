import styled, { css } from 'styled-components';
import {
  isDefined,
  isArray,
  isNumber,
  toArray,
  mediaQuery,
  toPx,
  spaceMixin,
  sizeMixin,
  breakpointMixin,
  flexMixin,
} from '../../../styled/utils';

export const Rowsc = styled.div`
  width: 100%;
  display: block;

  ${spaceMixin};
  ${flexMixin};
  ${props => breakpointMixin(props, 'display')};
  ${sizeMixin};

  ${props =>
    props.display === 'flex'
      ? css`
          display: flex;
          justify-content: ${props.justifyContent};
          flex-wrap: wrap;
        `
      : css`
          &:after {
            content: '';
            clear: both;
            display: table;
          }
        `};
`;
