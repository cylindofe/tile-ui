// @flow
import React from 'react';
import { Rowsc } from './styled';
import type { JustifyContent, FlexDirection, Display } from '../../../types';

type Props = {
  children: any,
  flex?: boolean,
  justifyContent?: JustifyContent,
  flexDirection?: FlexDirection,
  display?: Display,
};

const Row = ({ children, ...rest }: Props) => <Rowsc {...rest}>{children}</Rowsc>;

Row.defaultProps = {
  flex: true,
  justifyContent: 'flex-start',
  flexDirection: 'row',
  display: 'flex',
};

export default Row;
