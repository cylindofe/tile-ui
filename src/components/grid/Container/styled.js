import styled, { css } from 'styled-components';
import { lodash as _ } from '../../../index';
import {
  isDefined,
  isArray,
  isNumber,
  toArray,
  mediaQuery,
  toPx,
  spaceMixin,
  breakpointMixin,
  sizeMixin,
  flexMixin,
} from '../../../styled/utils';

export const ContainerSC = styled.div`
  position: relative;
  background-color: ${props => props.backgroundColor};
  overflow: ${props => props.overflow};
  position: relative;
  margin: 0px auto;

  ${spaceMixin};
  ${props => breakpointMixin(props, 'display')};
  ${sizeMixin};
  ${flexMixin};

  ${props =>
    props.stretch &&
    css`
      width: 100%;
      max-width: 100%;
      padding-left: 0px;
      padding-right: 0px;
    `};
`;
