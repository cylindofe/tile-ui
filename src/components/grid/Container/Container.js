// @flow
import * as React from 'react';
import { ContainerSC } from './styled';
import type { FlexDirection } from '../../../types';

type Props = {
  children: any,
  width?: any,
  height?: any,
  backgroundColor?: string,
  overflow?: string,
  maxWidth?: number,
  stretch?: boolean,
  padding?: any,
  paddingRight?: any,
  paddingLeft?: any,
  paddingBottom?: any,
  paddingTop?: any,
  display?: any,
  flex?: number,
  flexDirection?: FlexDirection,
};

export const Container = ({ children, ...rest }: Props) => <ContainerSC {...rest}>{children}</ContainerSC>;

Container.defaultProps = {
  backgroundColor: 'transparent',
  overflow: 'initial',
  height: null,
  width: null,
  maxWidth: 1200,
  stretch: false,
  padding: null,
  paddingRight: [8, 8, 8, 0],
  paddingLeft: [8, 8, 8, 0],
  paddingTop: 0,
  paddingBottom: 0,
  display: ' flex',
  flex: 1,
  flexDirection: 'column',
};
