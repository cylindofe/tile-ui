import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Container } from './Container';
import TILE from '../../../index';

it('Should render a default Container component', () => {
  const { theme } = TILE.uiStore;
  const component = <Container theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
