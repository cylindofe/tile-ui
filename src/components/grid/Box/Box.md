# Box

A simple component that can wrap other components. Use this to add margin or padding to nested components.
