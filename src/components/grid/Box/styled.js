import styled, { css } from 'styled-components';
import { spaceMixin, sizeMixin, positionMixin, breakpointMixin, flexMixin } from '../../../styled/utils';
import { lodash as _ } from '../../../index';

export const BoxSC = styled.div`
  box-shadow: ${props => props.theme.elevation[props.elevation]};
  border-radius: ${props => props.theme.borderRadius[props.borderRadius]}px;
  overflow: ${props => props.overflow};
  background-color: ${props =>
    props.theme.backgroundColor[props.backgroundColor] || props.backgroundColor || 'inherit'};

  ${spaceMixin};
  ${sizeMixin};
  ${positionMixin};
  ${props => breakpointMixin(props, 'display')};
  ${flexMixin};

  ${props =>
    props.display === 'flex' &&
    css`
      flex-wrap: wrap;
    `};
`;
