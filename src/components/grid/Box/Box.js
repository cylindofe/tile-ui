// @flow
import * as React from 'react';
import { BoxSC } from './styled';
import type { JustifyContent, FlexDirection, Display } from '../../../types';

type Props = {
  children: any,
  width?: any,
  height?: any,
  padding?: number,
  elevation?: number,
  borderRadius?: number,
  backgroundColor?: string,
  overflow?: string,
  stretch?: boolean,
  justifyContent?: JustifyContent,
  alignItems?: JustifyContent,
  flexDirection?: FlexDirection,
  display?: Display,
};

export const Box = ({ children, ...rest }: Props) => <BoxSC {...rest}>{children}</BoxSC>;

Box.defaultProps = {
  display: 'flex',
  padding: 0,
  elevation: 0,
  borderRadius: 0,
  backgroundColor: 'transparent',
  overflow: 'visible',
  height: null,
  width: null,
  stretch: false,
  justifyContent: 'flex-start',
  alignItems: 'flex-start',
  flexDirection: 'column',
};
