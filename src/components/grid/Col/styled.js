import styled, { css } from 'styled-components';
import {
  isDefined,
  isArray,
  isNumber,
  toArray,
  mediaQuery,
  toPx,
  spaceMixin,
  sizeMixin,
  breakpointMixin,
  flexMixin,
} from '../../../styled/utils';
import { lodash as _ } from '../../../index';

export const ColSC = styled.div`
  float: ${props => props.float};
  overflow: ${props => props.overflow};
  position: relative;

  ${props => spaceMixin(props)};
  ${props => breakpointMixin(props, 'width')};
  ${props => breakpointMixin(props, 'display')};
  ${flexMixin};
  ${sizeMixin};

  ${props =>
    props.floatContent &&
    css`
      > * {
        float: ${props.floatContent};
      }
    `};
`;
