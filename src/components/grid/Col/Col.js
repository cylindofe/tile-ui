// @flow
import * as React from 'react';
import { ColSC } from './styled';
import type { JustifyContent, FlexDirection, Display } from '../../../types';

type Props = {
  children: React.Node,
  floatContent?: any,
  justifyContent?: JustifyContent,
  flexDirection?: FlexDirection,
  overflow?: any,
  display?: Display,
  flex?: number,
};

const Col = ({ children, ...rest }: Props) => <ColSC {...rest}>{children}</ColSC>;

Col.defaultProps = {
  flex: 1,
  display: 'flex',
  justifyContent: 'flex-start',
  flexDirection: 'column',
  floatContent: null,
  overflow: null,
};

export default Col;
