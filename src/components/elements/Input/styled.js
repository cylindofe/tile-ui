import styled, { css } from 'styled-components';

const errorStyle = props => {
  if (props.hasError)
    return `
  box-shadow: 0 0 0 1px ${props.theme.color.danger};
  `;
};

const focusStyle = props => `
  box-shadow: 0 0 0 1px ${props.theme.color.primary};
  `;

const getTextareaHeight = ({ lines, theme }) => {
  const index = lines * 5;
  return `${theme.height[index]}px`;
};

export const InputWrapper = styled.div``;

export const InputSC = styled.input`
  height: ${props => props.theme.uiSize[10]}px;
  box-shadow: ${props => props.theme.elevation[1]};
  border: none;
  border-radius: ${props => props.theme.component.Button.borderRadius.default}px;
  width: 100%;
  outline: none;
  transition: all 200ms ease;
  padding-right: ${props => props.theme.padding[3]}px;
  padding-left: ${props => props.theme.padding[3]}px;
  padding-top: ${props => props.theme.padding[1]}px;
  padding-bottom: ${props => props.theme.padding[1]}px;
  font-size: 12px;

  &:focus {
    ${focusStyle};
  }

  ${errorStyle};
`;

export const TextareaSC = styled.textarea`
  min-height: ${getTextareaHeight};
  box-shadow: ${props => props.theme.elevation[1]};
  border: none;
  border-radius: ${props => props.theme.component.Button.borderRadius.default}px;
  width: 100%;
  outline: none;
  transition: all 200ms ease;
  padding-right: ${props => props.theme.padding[3]}px;
  padding-left: ${props => props.theme.padding[3]}px;
  padding-top: ${props => props.theme.padding[2]}px;
  padding-bottom: ${props => props.theme.padding[1]}px;
  resize: none;
  font-size: 12px;

  &:focus {
    ${focusStyle};
  }

  ${errorStyle};
`;
