// @flow
import * as React from 'react';
import { InputSC, InputWrapper, TextareaSC } from './styled';
import { lodash as _, Label } from '../../../index';

type Props = {
  value: string,
  onChange: Function,
  lines?: number,
  placeholder: string,
};

export const Input = ({ lines = 1, ...rest }: Props) => (
  <InputWrapper>
    {lines > 1 ? <TextareaSC lines={lines} type="text" {...rest} /> : <InputSC type="text" {...rest} />}
  </InputWrapper>
);

Input.defaultProps = {
  lines: 1,
};
