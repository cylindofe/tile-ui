import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Input } from './Input';
import TILE from '../../../index';

it('Should render a default Input component', () => {
  const { theme } = TILE.uiStore;
  const component = <Input theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
