// @flow
import * as React from 'react';

import LogomarkColored from './logomark_colored.svg';
import LogomarkInverted from './logomark_inverted.svg';
import LogomarkDark from './logomark_dark.svg';

import LogotypeColored from './logotype_colored.svg';
import LogotypeInverted from './logotype_inverted.svg';
import LogotypeDark from './logotype_dark.svg';

import { LogoSC } from './styled';

type Props = {
  type?: 'logomark' | 'logotype',
  size?: number,
  padding?: number,
  color?: 'colored' | 'inverted' | 'dark',
};

export const Logo = ({ type, color, ...rest }: Props) => {
  let aspectRatio;

  switch (type) {
    case 'logotype':
      aspectRatio = 2.8;

      return (
        <LogoSC aspectRatio={aspectRatio} {...rest}>
          {color === 'colored' && <LogotypeColored />}
          {color === 'inverted' && <LogotypeInverted />}
          {color === 'dark' && <LogotypeDark />}
        </LogoSC>
      );

    case 'logomark':
      aspectRatio = 1;

      return (
        <LogoSC aspectRatio={aspectRatio} {...rest}>
          {color === 'colored' && <LogomarkColored />}
          {color === 'inverted' && <LogomarkInverted />}
          {color === 'dark' && <LogomarkDark />}
        </LogoSC>
      );

    default:
      console.log('No type matches');
      break;
  }
};

Logo.defaultProps = {
  size: 10,
  padding: 1,
  type: 'logotype',
  color: 'colored',
};
