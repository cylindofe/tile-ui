import styled, { css } from 'styled-components';

export const LogoSC = styled.div`
  background-size: contain;
  height: ${props => props.theme.uiSize[props.size]}px;
  width: ${props => props.theme.uiSize[props.size] * props.aspectRatio}px;

  svg {
    height: 100%;
    width: 100%;
    padding: ${props => props.theme.uiSize[props.padding]};
  }
`;
