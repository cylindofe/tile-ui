// @flow
import * as React from 'react';
import { FormSC } from './styled';
import { Row, SubmitButton } from '../../../index';

type Props = {
  onSubmit: Function,
  children: any,
  noSubmit: boolean,
  isSubmitting: boolean,
  submitLabel?: string,
};

export const Form = ({ onSubmit, isSubmitting, children, submitLabel, noSubmit, ...rest }: Props) => (
  <FormSC onKeyDown={e => e.key === 'Enter' && e.preventDefault()} onSubmit={onSubmit}>
    {children}

    {!noSubmit && (
      <Row justifyContent="flex-end">
        <SubmitButton onClick={onSubmit} disabled={isSubmitting}>
          {submitLabel}
        </SubmitButton>
      </Row>
    )}
  </FormSC>
);

Form.defaultProps = {
  submitLabel: 'Submit',
};
