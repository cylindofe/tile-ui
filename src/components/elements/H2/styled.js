import styled, { css } from 'styled-components';

export const H2 = styled.h2`
  font-size: ${props =>
    props.fontSize || props.fontSize === 0
      ? props.theme.fontSize[props.fontSize]
      : props.theme.component.H2.fontSize.default}px;
  color: ${props => props.theme.component.H2.color[props.color] || props.theme.color[props.color] || props.color};
  margin-bottom: ${props =>
    props.marginBottom || props.marginBottom === 0
      ? props.theme.uiSize[props.marginBottom]
      : props.theme.component.H2.marginBottom.default}px;

  font-weight: ${props => props.theme.component.H2.fontWeight[props.fontWeight]};
  line-height: ${props =>
    props.multiLine ? props.theme.component.H2.lineHeight.multiLine : props.theme.component.H2.lineHeight.default}px;
  text-align: ${props => props.theme.component.H2.textAlign[props.textAlign]};

  ${props =>
    props.color &&
    css`
      color: ${props.theme.component.H2.color[props.color]};
    `};
`;
