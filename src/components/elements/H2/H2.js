// @flow
import * as React from 'react';
import PropTypes from 'prop-types';
import { H2 as H2CS } from './styled';

type Props = {
  children: any,
  multiLine?: boolean,
  textAlign?: 'default' | 'center' | 'right',
  fontWeight?: 'default' | 'bold',
  color?: string,
};

const H2 = ({ children, ...rest }: Props) => <H2CS {...rest}>{children}</H2CS>;

/**
 * defaultProps
 */
H2.defaultProps = {
  color: 'default',
  textAlign: 'default',
  multiLine: true,
  fontWeight: 'default',
};

export default H2;
