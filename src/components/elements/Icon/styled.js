import styled from 'styled-components';
import { spaceMixin } from '../../../styled/utils';
import { utils } from '../../../index';

export const Icon = styled.i`
  display: inline-block;
  vertical-align: top;
  width: ${({ size }) => size}px;
  height: ${({ size }) => size}px;

  svg {
    fill: ${({ color, theme }) => {
      if (theme.color[color]) return theme.color[color];
      return color;
    }};
  }

  ${props => spaceMixin(props)};
`;
