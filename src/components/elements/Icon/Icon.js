// @flow
import * as React from 'react';
import { type Theme } from 'styled-components';
import svgPaths, { questionmark as defaultIcon } from './svgPaths';
import { Icon as IconComp } from './styled';
import { UIcolors } from '../../../constants';

type Props = {
  icon: $Keys<typeof svgPaths>,
  size?: number,
  color?: $Keys<typeof UIcolors>,
};

const Icon = ({ icon, size, color, ...rest }: Props) => {
  const path = svgPaths[icon] || defaultIcon;

  if (!path) return null;

  return (
    <IconComp size={size} color={color} {...rest}>
      <svg width={size} height={size} viewBox="0 0 24 24">
        <path d={path} />
      </svg>
    </IconComp>
  );
};
/**
 * defaultProps
 */
Icon.defaultProps = {
  icon: 'questionmark',
  size: 20,
  color: UIcolors.grey,
};

export default Icon;
