import styled, { css } from 'styled-components';
import { spaceMixin, fontMixin, addProperty } from '../../../styled/utils';

export const TextSC = styled.span`
  ${fontMixin};
  ${spaceMixin};
  ${props => addProperty(props, 'display')};
  ${props => addProperty(props, 'backgroundColor')};

  line-height: ${({ fontSize, theme }) => theme.fontSize[fontSize]}px;
`;
