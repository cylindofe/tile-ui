import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Text } from './Text';
import TILE from '../../../index';

it('Should render a default Text component', () => {
  const { theme } = TILE.uiStore;
  const component = <Text theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
