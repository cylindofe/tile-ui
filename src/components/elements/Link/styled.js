import styled, { css } from 'styled-components';
import { spaceMixin, addProperty } from '../../../styled/utils';

export const LinkSC = styled.a`
  display: ${props => props.display};
  cursor: pointer;
  height: ${props => (props.height ? props.theme.uiSize[props.height] : 'auto')}px;
  line-height: ${props => (props.height ? props.theme.uiSize[props.height] : 'inherit')}px;
  color: ${props => props.theme.color[props.color] || props.color || 'inherit'};
  text-decoration: none;
  transition: all 200ms ease;
  font-weight: ${props => (props.active ? props.theme.fontWeight[3] : props.theme.fontWeight[2])};
  opacity: 1;

  ${props => addProperty(props, 'fontSize')};

  ${props => {
    switch (props.transition) {
      case 'opacity':
        return css`
          &:hover {
            opacity: 0.8;
          }
        `;
      case 'scale':
        return css`
          transform: scale(1);
          &:hover {
            transform: scale(1.01);
            opacity: 0.9;
          }
        `;
      default:
        return null;
    }
  }};

  ${props => spaceMixin(props)};
`;
