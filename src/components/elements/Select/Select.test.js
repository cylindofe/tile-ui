import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Select } from './Select';
import TILE from '../../../index';

it('Should render a default Select component', () => {
  const { theme } = TILE.uiStore;
  const component = <Select theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
