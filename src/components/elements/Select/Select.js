// @flow
import * as React from 'react';
import { lodash as _, P, Icon } from '../../../index';

import { SelectWrapper, SelectInput, SelectContainer, SelectItemList, SelectItemSC, HiddenInput } from './styled';
import { SelectItem } from './SelectItem';

type Option = {
  value: any,
  label: string,
  id: string,
};

type Props = {
  value: any,
  options: Array<Option>,
  onChange: Function,
  id: string,
};

type State = {
  open: boolean,
  focused: boolean,
  hovering: boolean,
};

export class Select extends React.Component<Props, State> {
  state = {
    open: false,
    focused: false,
    hovering: false,
  };

  static defaultProps = {};

  hiddenInputRef: any = React.createRef();

  toggleOpen = () => {
    const { open } = this.state;
    if (!open) this.focusInput();
    this.setState({
      open: !open,
    });
  };

  setOpen = (value: boolean) => {
    this.setState({
      open: value,
    });
  };

  setFocus = (value: boolean) => {
    if (!value) this.setOpen(false);
    this.setState({
      focused: value,
    });
  };

  focusInput = () => {
    this.hiddenInputRef.current.focus();
  };

  blurInput = () => {
    this.hiddenInputRef.current.blur();
  };

  onItemClick = (e: any, o: Option) => {
    const { onChange, id } = this.props;
    onChange(id, o);
    this.setOpen(false);
  };

  onKeyDown = (e: any) => {
    switch (e.key) {
      case 'Tab':
        this.setFocus(false);
        break;
      case 'Enter':
      case 'Space':
      case 'ArrowDown':
      case 'ArrowUp':
        this.toggleOpen();
        break;
      default:
        break;
    }
  };

  onBlur = () => {
    const { hovering } = this.state;
    if (!hovering) {
      this.setFocus(false);
      this.blurInput();
    } else {
      this.focusInput();
    }
  };

  onFocus = () => {
    this.setFocus(true);
  };

  setHovering = (value: boolean) => {
    this.setState({
      hovering: value,
    });
  };

  render() {
    const { value, options } = this.props;
    const { open, focused } = this.state;

    return (
      <SelectWrapper onMouseEnter={() => this.setHovering(true)} onMouseLeave={() => this.setHovering(false)}>
        <HiddenInput
          onKeyDown={this.onKeyDown}
          innerRef={this.hiddenInputRef}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
        />
        <SelectInput focused={focused} onClick={this.toggleOpen}>
          <P marginRight={5} lineHeight={10} marginBottom={0}>
            {value.label}
          </P>
          <Icon size={16} marginLeft="auto" icon="caretDown" />
        </SelectInput>
        <SelectContainer open={open}>
          <SelectItemList>
            {_.map(options, (o, i) => <SelectItem key={o.id || i} onClick={e => this.onItemClick(e, o)} {...o} />)}
          </SelectItemList>
        </SelectContainer>
      </SelectWrapper>
    );
  }
}
