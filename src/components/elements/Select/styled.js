import styled, { css } from 'styled-components';
import { lighten, darken, transparentize } from 'polished';

export const SelectInput = styled.div`
  cursor: pointer;
  height: ${props => props.theme.uiSize[10]}px;
  box-shadow: ${props => props.theme.elevation[1]};
  border: none;
  border-radius: ${props => props.theme.component.Button.borderRadius.default}px;
  width: 100%;
  outline: none;
  transition: all 200ms ease;
  padding-right: ${props => props.theme.padding[3]}px;
  padding-left: ${props => props.theme.padding[3]}px;
  background: white;
  display: flex;
  align-items: center;

  ${props =>
    props.focused &&
    css`
      box-shadow: 0 0 0 1px ${props.theme.color.primary};
    `};
`;

export const SelectWrapper = styled.div`
  position: relative;
`;

export const SelectContainer = styled.div`
  width: 100%;
  opacity: 0;
  position: absolute;
  transition: all 200ms ease;
  background: white;
  box-shadow: ${props => props.theme.elevation[5]};
  border-radius: ${props => props.theme.borderRadius[2]}px;
  overflow: hidden;
  pointer-events: none;
  top: 44px;

  ${({ open }) =>
    open &&
    css`
      pointer-events: all;
      opacity: 1;
      z-index: 9;
    `};
`;

export const SelectItemSC = styled.li`
  border-bottom: 1px solid #e7ecf3;
  padding-left: ${props => props.theme.uiSize[2]}px;
  padding-right: ${props => props.theme.uiSize[2]}px;
  font-weight: 400;
  font-size: 17px;
  line-height: 26px;

  &:hover {
    background: ${props => lighten(0.425, props.theme.color.primary)};
    cursor: pointer;
  }
`;

export const HiddenInput = styled.input`
  pointer-events: none;
  opacity: 0;
  position: absolute;
`;

export const SelectItemList = styled.ul``;

export const DropdownIcon = styled.div``;
