// @flow
import * as React from 'react';
import { SelectItemSC } from './styled';
import { P } from '../../../index';

type Props = {
  label: string,
  onClick: Function,
};

export const SelectItem = ({ label, onClick }: Props) => (
  <SelectItemSC onClick={onClick}>
    <P lineHeight={10} marginBottom={0}>
      {label}
    </P>
  </SelectItemSC>
);

SelectItem.defaultProps = {};
