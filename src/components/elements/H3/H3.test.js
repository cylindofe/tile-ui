import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { H3 } from './H3';
import TILE from '../../../index';

it('Should render a default H3 component', () => {
  const { theme } = TILE.uiStore;
  const component = <H3 theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
