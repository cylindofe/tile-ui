import styled, { css } from 'styled-components';

export const H3 = styled.h3`
  font-size: ${props =>
    props.fontSize || props.fontSize === 0
      ? props.theme.fontSize[props.fontSize]
      : props.theme.component.H3.fontSize.default}px;
  color: ${props => props.theme.component.H3.color[props.color] || props.theme.color[props.color] || props.color};
  margin-bottom: ${props =>
    props.marginBottom || props.marginBottom === 0
      ? props.theme.uiSize[props.marginBottom]
      : props.theme.component.H3.marginBottom.default}px;

  font-weight: ${props => props.theme.component.H3.fontWeight[props.fontWeight]};
  line-height: ${props =>
    props.multiLine ? props.theme.component.H3.lineHeight.multiLine : props.theme.component.H3.lineHeight.default}px;
  text-align: ${props => props.theme.component.H3.textAlign[props.textAlign]};

  ${props =>
    props.color &&
    css`
      color: ${props.theme.component.H3.color[props.color]};
    `};
`;
