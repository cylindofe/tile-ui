// @flow
import * as React from 'react';
import PropTypes from 'prop-types';
import { H3 as H3CS } from './styled';

type Props = {
  children: any,
  multiLine?: boolean,
  textAlign?: 'default' | 'center' | 'right',
  fontWeight?: 'default' | 'bold',
  color?: 'white' | 'default' | string,
};

export const H3 = ({ children, ...rest }: Props) => <H3CS {...rest}>{children}</H3CS>;

/**
 * defaultProps
 */
H3.defaultProps = {
  color: 'default',
  textAlign: 'default',
  multiLine: true,
  fontWeight: 'default',
};
