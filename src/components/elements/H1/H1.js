// @flow
import * as React from 'react';
import PropTypes from 'prop-types';
import { H1sc } from './styled';
import { fontWeight } from '../../../theme/themeValues';

type Props = {
  textAlign?: 'default' | 'center' | 'right',
  children: any,
  fontWeight?: 'bold' | 'default',
  color?: string,
  multiLine?: boolean,
  fontSize?: number,
};

const H1 = ({ children, ...rest }: Props) => <H1sc {...rest}>{children}</H1sc>;

H1.defaultProps = {
  color: 'text',
  textAlign: 'default',
  fontWeight: 'default',
  multiLine: true,
  fontSize: 7,
};

export default H1;
