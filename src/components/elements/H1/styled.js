import styled, { css } from 'styled-components';
import { spaceMixin, sizeMixin, breakpointMixin, fontMixin } from '../../../styled/utils';

export const H1sc = styled.h1`
  ${spaceMixin};
  ${fontMixin};
`;
