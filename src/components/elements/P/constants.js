// @flow
export const pSizes = {
  xsmall: 'xsmall',
  small: 'small',
  normal: 'normal',
};

export const pStyle = {
  normal: 'normal',
  italic: 'italic',
};

export const pWeight = {
  thin: 'thin',
  normal: 'normal',
  bold: 'bold',
};
