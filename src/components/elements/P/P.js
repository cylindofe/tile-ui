// @flow

import * as React from 'react';
import PropTypes from 'prop-types';
import { Psc } from './styled';

type Props = {
  children: any,
  fontSize?: string | number,
  center?: boolean,
  overflow?: any,
};

const P = ({ children, ...rest }: Props) => <Psc {...rest}>{children}</Psc>;

/**
 * defaultProps
 */
P.defaultProps = {
  center: false,
  fontSize: 'default',
  overflow: null,
};

export default P;
