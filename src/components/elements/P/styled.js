import styled, { css } from 'styled-components';
import { pSizes, pStyle, pWeight } from './constants';
import { lodash as _ } from '../../../index';
import { breakpointMixin, spaceMixin, sizeMixin } from '../../../styled/utils';

export const Psc = styled.p`
  color: ${props =>
    props.theme.component.P.color[props.color] ||
    props.theme.color[props.color] ||
    props.theme.component.P.color.default ||
    props.color ||
    'inherit'};
  margin-top: 0px;
  font-size: ${props =>
    _.isNumber(props.fontSize)
      ? props.theme.fontSize[props.fontSize]
      : props.theme.component.P.fontSize[props.fontSize]}px;
  margin-bottom: ${props =>
    props.marginBottom || props.marginBottom === 0
      ? props.theme.uiSize[props.marginBottom]
      : props.theme.component.P.marginBottom.default}px;
  text-align: ${props => props.textAlign};

  ${props => breakpointMixin(props, 'lineHeight')};
  ${spaceMixin};
  ${sizeMixin};

  /*
   * Font style
   */
  ${props =>
    props.fontStyle === pStyle.normal &&
    css`
      font-style: normal;
    `};
  ${props =>
    props.fontStyle === pStyle.italic &&
    css`
      font-style: italic;
    `};

  /*
   * Font Weight
   */
  ${props =>
    props.fontWeight === pWeight.thin &&
    css`
      font-weight: 100;
    `};
  ${props =>
    props.fontWeight === pWeight.normal &&
    css`
      font-weight: 400;
    `};
  ${props =>
    props.fontWeight === pWeight.bold &&
    css`
      font-weight: 600;
    `};

  /*
   * Text transforms
   */
  ${props =>
    props.textTransform &&
    css`
      text-transform: ${props.textTransform};
    `};

  ${props =>
    props.color &&
    css`
      color: ${props.theme.component.P.color[props.color]};
    `};

  ${props =>
    props.overflow &&
    css`
      overflow: hidden;
      display: -webkit-box;
      -webkit-line-clamp: 3;
      -webkit-box-orient: vertical;
      text-overflow: ellipsis;
    `};
`;
