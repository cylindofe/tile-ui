import * as React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import P from './P';
import TILE from '../../../index';

it('Should render an P tag', () => {
  const { theme } = TILE.uiStore;
  const defaults = theme.component.P;

  const component = <P theme={theme}>Hello!</P>;
  const tree = renderer.create(component).toJSON();

  expect(tree).toHaveStyleRule('color', defaults.color.default);
  expect(tree).toMatchSnapshot();
});
