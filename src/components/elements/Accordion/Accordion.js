// @flow
import * as React from 'react';
import { AccordionSC, Item, Title, AccordionBody as Body } from './styled';
import { lodash as _, Box } from '../../../index';

type ItemType = {
  height: number,
  id: string,
  title: string,
  render: () => {},
  startToggled: boolean,
};

type Props = {
  items: [ItemType],
  forceSingleToggle: boolean,
  backgroundColor: string,
};

type State = {
  toggled: {},
  total: number,
};
class Accordion extends React.Component<Props, State> {
  state = {
    toggled: {},
    total: 0,
  };

  static defaultProps = {
    forceSingleToggle: true,
    backgroundColor: '#fff',
  };

  componentDidMount() {
    const { items } = this.props;

    if (items.length) {
      this.buildState(items);
    }
  }

  buildState = (items: any) => {
    const toggled = {};

    _.forEach(items, (obj: ItemType) => {
      const initalValue = obj.startToggled || false;
      _.set(toggled, `${obj.id}`, initalValue);
    });

    this.setState({
      toggled,
      total: items.length,
    });
  };

  toggleItem = (id: any) => {
    const { forceSingleToggle } = this.props;

    this.setState(({ toggled }) => {
      const nextToggled = { ...toggled };

      /**
       * If forceSingleToggle is enabled,
       * only allow one item to be toggled open.
       */
      if (forceSingleToggle) {
        _.forEach(Object.keys(nextToggled), (key, i) => {
          nextToggled[key] = false;
        });
      }

      _.set(nextToggled, `${id}`, !_.get(toggled, id));

      return {
        toggled: nextToggled,
      };
    });
  };

  render() {
    const { items, backgroundColor } = this.props;
    const { toggled, total } = this.state;

    return (
      <AccordionSC>
        <Box backgroundColor={backgroundColor} padding={0} elevation={2}>
          {_.map(items, (obj, i) => (
            <Item last={total === i + 1} key={obj.id}>
              <Title onClick={() => this.toggleItem(obj.id)} toggled={toggled[obj.id]}>
                {obj.title}
              </Title>
              <Body height={obj.height} toggled={toggled[obj.id]}>
                {obj.render()}
              </Body>
            </Item>
          ))}
        </Box>
      </AccordionSC>
    );
  }
}

export default Accordion;
