import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import Accordion from './Accordion';
import TILE from '../../../index';

it('Should render a default Accordion component', () => {
  const { theme } = TILE.uiStore;
  const component = <Accordion theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
