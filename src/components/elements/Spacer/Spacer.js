// @flow
import * as React from 'react';
import { SpacerSC } from './styled';

type Props = {
  height?: number,
};

export const Spacer = ({ height, ...rest }: Props) => <SpacerSC height={height} {...rest} />;

Spacer.defaultProps = {
  height: 20,
};
