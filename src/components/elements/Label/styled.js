import styled from 'styled-components';
import { breakpointMixin, fontMixin, spaceMixin } from '../../../styled/utils';

export const Label = styled.label`
  font-weight: 500;
  font-size: 12px;
  color: ${props => props.theme.color.label};
  line-height: 12px;

  ${props => breakpointMixin(props, 'display')};
  ${fontMixin};
  ${spaceMixin};
`;
