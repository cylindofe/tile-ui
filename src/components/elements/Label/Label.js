// @flow
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Label } from './styled';

type Props = {
  display?: any,
  children: any,
};

const _Label = ({ children, ...rest }: Props) => <Label {...rest}>{children}</Label>;

/**
 * propTypes
 */
_Label.defaultProps = {
  display: 'block',
};

export default _Label;
