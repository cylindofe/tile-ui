import * as React from 'react';
import { SeparatorSC } from './styled';

export const Separator = props => <SeparatorSC {...props} />;
