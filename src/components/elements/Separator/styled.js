import styled from 'styled-components';
import { spaceMixin } from '../../../styled/utils';

export const SeparatorSC = styled.hr`
  color: ${props =>
    props.theme.component.Separator.color[props.color] ||
    props.theme.color[props.color] ||
    props.theme.component.Separator.color.default ||
    props.color ||
    'inherit'};
  background-color: ${props =>
    props.theme.component.Separator.color[props.color] ||
    props.theme.color[props.color] ||
    props.theme.component.Separator.color.default ||
    props.color ||
    'inherit'};
  height: 1px;
  width: 100%;
  border: none;
  margin-top: ${props => props.theme.uiSize[props.space || 4]}px;
  margin-bottom: ${props => props.theme.uiSize[props.space || 4]}px;
  ${props => spaceMixin(props)};
`;
