import styled, { css } from 'styled-components';
import { lighten, darken, transparentize } from 'polished';
import { addProperty, getValue } from '../../../styled/utils';

export const TagSC = styled.span`
  padding: 0 12px;
  height: 20px;
  border-radius: 50px;
  font-weight: 500;
  font-size: 12px;
  line-height: 20px;
  white-space: nowrap;
  display: inline-block;

  text-transform: uppercase;
  font-size: 8px;
  letter-spacing: 1px;
  font-weight: 600;

  ${props => props.tagType === 'solid' && addProperty(props, 'backgroundColor')};
  ${props => props.tagType === 'solid' && addProperty(props, 'color')};

  ${({ theme, tagType, backgroundColor }) => {
    if (tagType !== 'transparent') return null;
    const color = getValue({ theme }, 'backgroundColor', backgroundColor);

    if (!color) return null;

    return css`
      background-color: ${transparentize(0.8, color)};
      color: ${color};
    `;
  }};
`;
