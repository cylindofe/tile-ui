import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Tag } from './Tag';
import TILE from '../../../index';

it('Should render a default Tag component', () => {
  const { theme } = TILE.uiStore;
  const component = <Tag theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
