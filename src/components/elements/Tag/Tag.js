// @flow
import * as React from 'react';
import { TagSC } from './styled';
import type { ThemeColors } from '../../../types';

type Props = {
  children: string,
  backgroundColor?: ThemeColors | string,
  color?: ThemeColors | string,
  tagType?: 'solid' | 'transparent',
};

export const Tag = ({ children, ...rest }: Props) => <TagSC {...rest}>{children}</TagSC>;

Tag.defaultProps = {
  backgroundColor: 'primary',
  color: 'white',
  tagType: 'solid',
};
