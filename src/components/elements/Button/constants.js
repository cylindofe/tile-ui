// @flow
export const buttonTypes = {
  raised: 'raised',
  hollow: 'hollow',
  flat: 'flat',
};

export const buttonShapes = {
  square: 'square',
  round: 'round',
};
