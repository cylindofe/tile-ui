import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import Button from './Button';
import TILE from '../../../index';

it('Should render a default Button', () => {
  const { theme } = TILE.uiStore;
  const defaults = theme.component.Button;

  const component = <Button theme={theme}>Hello!</Button>;
  const tree = renderer.create(component).toJSON();
  // expect(tree).toHaveStyleRule('background', colors.primary);
  expect(tree).toMatchSnapshot();
});

it('Should render a raised Button', () => {
  const { theme } = TILE.uiStore;
  const defaults = theme.component.Button;

  const component = (
    <Button theme={theme} buttonType="raised">
      I'm raised
    </Button>
  );
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Should render a hollow Button', () => {
  const { theme } = TILE.uiStore;
  const defaults = theme.component.Button;

  const component = (
    <Button theme={theme} buttonType="hollow">
      I'm hollow
    </Button>
  );
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Should render a flat Button', () => {
  const { theme } = TILE.uiStore;
  const defaults = theme.component.Button;

  const component = (
    <Button theme={theme} buttonType="flat">
      I'm flat
    </Button>
  );
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Should render a fallback button when supplying wrong type', () => {
  const { theme } = TILE.uiStore;
  const defaults = theme.component.Button;

  const component = (
    <Button theme={theme} buttonType="blabla">
      I'm nothing
    </Button>
  );
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Should render a disabled button', () => {
  const { theme } = TILE.uiStore;
  const defaults = theme.component.Button;

  const component = (
    <Button theme={theme} disabled>
      I'm disabled
    </Button>
  );
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
