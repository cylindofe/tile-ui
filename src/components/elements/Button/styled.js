import styled, { css } from 'styled-components';
import { lighten, darken, transparentize } from 'polished';
import { disabled, transition, focused, focusShadow } from '../../../styled';
import { buttonShapes } from './constants';
import { spaceMixin, sizeMixin } from '../../../styled/utils';

const spaceMixinConfig = {
  padding: false,
  paddingLeft: false,
  paddingRight: false,
  paddingTop: false,
  paddingBottom: false,
};

const Base = styled.button`
  border: none;
  border-radius: ${props =>
    props.shape === buttonShapes.round
      ? props.theme.component.Button.borderRadius.round
      : props.theme.component.Button.borderRadius.default}px;
  padding: 0px ${props => !props.onlyIcon && props.theme.uiSize[5]}px;
  height: ${props =>
    props.size === 'large' ? props.theme.uiSize[10] : props.size === 'small' ? props.theme.uiSize[8] : null}px;
  text-align: center;
  outline: none;
  cursor: pointer;
  font-size: 12px;
  user-select: none;
  font-weight: ${props => props.theme.fontWeight[2]};
  white-space: nowrap;
  line-height: 0;

  display: flex;
  align-items: center;
  justify-content: center;

  ${props => spaceMixin(props, spaceMixinConfig)};
  ${props => sizeMixin(props, 'width')};

  ${props => props.disabled && disabled};
  ${transition};

  ${props =>
    props.onlyIcon &&
    css`
      width: ${props.theme.uiSize[10]}px;
    `};

  ${props =>
    props.stretch &&
    css`
      width: 100%;
    `};

  &:focus {
    ${focused};
  }

  i {
    margin-right: ${props => !props.onlyIcon && props.theme.uiSize[2]}px;
  }
`;

/**
 * Raised
 */

const raisedActive = css`
  background: ${({ theme, color }) => lighten(0.05, theme.color[color])};
  box-shadow: ${props => props.theme.elevation[2]} ${focusShadow};
`;

export const RaisedButton = styled(Base)`
  background: ${props => props.theme.color[props.color]};
  box-shadow: ${props => props.theme.elevation[3]};
  color: white;

  &:hover,
  &:focus {
    ${raisedActive};
  }

  ${props => props.active && raisedActive};

  &:focus {
    box-shadow: ${focusShadow};
    background: ${({ theme, color }) => darken(0.07, theme.color[color])};
  }

  i > svg {
    fill: white;
  }
`;

/**
 * Hollow
 */
const hollowActive = css`
  background: ${({ theme, color }) => transparentize(0.9, theme.color[color])};
`;
export const HollowButton = styled(Base)`
  color: ${({ theme, color }) => theme.color[color]};
  box-shadow: 0px 0px 0px 1px ${({ theme, color }) => transparentize(0.6, theme.color[color])};
  background: transparent;

  &:hover,
  &:focus {
    ${hollowActive};
  }
  &:focus {
    ${focused};
  }

  ${props => props.active && hollowActive};

  i > svg {
    fill: ${({ theme, color }) => theme.color[color]};
  }
`;

/**
 * Flat
 */
const flatActive = css`
  background: ${({ theme, color }) => transparentize(0.9, theme.color[color])};
`;

export const FlatButton = styled(Base)`
  color: ${({ theme, color }) => theme.color[color]};
  background: transparent;

  &:hover,
  &:focus {
    ${flatActive};
  }

  ${props => props.active && flatActive};

  &:focus {
    ${focused};
  }

  i > svg {
    fill: ${({ theme, color }) => theme.color[color]};
  }
`;
