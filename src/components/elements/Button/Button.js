// @flow
import * as React from 'react';
import { RaisedButton, HollowButton, FlatButton } from './styled';
import { buttonTypes, buttonShapes } from './constants';
import { UIcolors } from '../../../constants';
import { Icon } from '../../../index';
import { width } from '../../../theme/themeValues';

type Props = {
  children?: React.Node,
  buttonType?: $Keys<typeof buttonTypes>,
  icon?: string,
  disabled?: boolean,
  onClick: any,
  shape?: $Keys<typeof buttonShapes>,
  color?: $Keys<typeof UIcolors>,
  active?: boolean,
  stretch?: boolean,
  width?: any,
  size?: 'large' | 'small',
};

const Button = ({ children, buttonType, icon, ...rest }: Props) => {
  const onlyIcon = icon && !children;

  const { onClick } = rest;

  const content = () => (
    <React.Fragment>
      {icon && <Icon size={16} icon={icon} />}
      {children}
    </React.Fragment>
  );

  switch (buttonType) {
    case buttonTypes.raised:
      return (
        <RaisedButton onKeyDown={e => e.key === 'Enter' && onClick()} onlyIcon={onlyIcon} {...rest}>
          {content()}
        </RaisedButton>
      );
    case buttonTypes.hollow:
      return (
        <HollowButton onKeyDown={e => e.key === 'Enter' && onClick()} onlyIcon={onlyIcon} {...rest}>
          {content()}
        </HollowButton>
      );
    case buttonTypes.flat:
      return (
        <FlatButton onKeyDown={e => e.key === 'Enter' && onClick()} onlyIcon={onlyIcon} {...rest}>
          {content()}
        </FlatButton>
      );
    default:
      console.log('unknown button type');
      return (
        <RaisedButton onKeyDown={e => e.key === 'Enter' && onClick()} onlyIcon={onlyIcon} {...rest}>
          {content()}
        </RaisedButton>
      );
  }
};

/**
 * defaultProps
 */
Button.defaultProps = {
  color: UIcolors.primary,
  buttonType: buttonTypes.raised,
  shape: buttonShapes.square,
  stretch: false,
  width: null,
  size: 'large',
};

export default Button;
