import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { HoverTransition } from './HoverTransition';
import TILE from '../../../index';

it('Should render a default HoverTransition component', () => {
  const { theme } = TILE.uiStore;
  const component = <HoverTransition theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
