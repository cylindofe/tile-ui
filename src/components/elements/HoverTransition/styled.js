import styled, { css } from 'styled-components';

export const HoverTransitionSC = styled.div`
  transition: all 200ms ease-in-out;
  cursor: pointer;

  ${props => {
    switch (props.transition) {
      case 'opacity':
        return css`
          &:hover {
            opacity: 0.8;
          }
        `;
      case 'scale':
        return css`
          transform: scale(1);
          &:hover {
            transform: scale(1.01);
            opacity: 0.9;
          }
        `;
      default:
        return null;
    }
  }};
`;
