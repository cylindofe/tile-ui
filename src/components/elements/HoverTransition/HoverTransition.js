// @flow
import * as React from 'react';
import { HoverTransitionSC } from './styled';

type Props = {
  children: any,
  transition?: 'opacity' | 'scale',
};

export const HoverTransition = ({ children, ...rest }: Props) => (
  <HoverTransitionSC {...rest}>{children}</HoverTransitionSC>
);

HoverTransition.defaultProps = {
  transition: 'opacity',
};
