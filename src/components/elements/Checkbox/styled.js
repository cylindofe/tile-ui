import styled, { css } from 'styled-components';

export const CheckboxSC = styled.input`
  height: 24px;
  width: 24px;
  opacity: 0;
  cursor: pointer;
`;

export const CheckboxOuter = styled.div`
  height: 40px;
  width: 40px;
  position: relative;
  cursor: pointer;
  display: flex;
  justify-items: center;
  justify-content: center;
  align-items: center;

  &:hover {
    > div {
      border-color: ${({ theme }) => theme.color.primary};
    }
  }
`;

export const CheckboxUI = styled.div`
  position: absolute;
  height: 24px;
  width: 24px;
  border-radius: 24px;
  border: 1px solid ${({ theme }) => theme.color.border};
  pointer-events: none;
  cursor: pointer;
  background: white;
  transition: all 200ms ease;
  display: flex;
  align-content: center;
  justify-content: center;

  ${({ checked, theme }) =>
    checked &&
    css`
      box-shadow: ${props => props.theme.elevation[3]};
      background: ${theme.color.primary};
      border: 1px solid ${theme.color.primary};
    `};
`;
