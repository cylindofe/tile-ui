import * as React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { Checkbox } from './Checkbox';
import TILE from '../../../index';

it('Should render a default Checkbox component', () => {
  const { theme } = TILE.uiStore;
  const component = <Checkbox theme={theme} />;
  const tree = renderer.create(component).toJSON();
  expect(tree).toMatchSnapshot();
});
