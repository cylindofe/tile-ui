// @flow
import * as React from 'react';
import { CheckboxSC, CheckboxOuter, CheckboxUI } from './styled';
import { Icon } from '../../../index';

type Props = {
  id: string,
  value?: boolean,
  onChange: Function,
};

export const Checkbox = ({ value, onChange, ...rest }: Props) => (
  <CheckboxOuter>
    <CheckboxSC type="checkbox" checked={value} onChange={onChange} {...rest} />
    <CheckboxUI checked={value}>{value && <Icon color="white" icon="checkmark" />}</CheckboxUI>
  </CheckboxOuter>
);

Checkbox.defaultProps = {
  value: false,
};
