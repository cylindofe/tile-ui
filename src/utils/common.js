import { lodash as _ } from '../index';

export const isString = s => typeof s === 'string' || s instanceof String;
export const isDefined = n => n !== undefined && n !== null;
export const isNumber = n => _.isNumber(n);
export const isArray = n => Array.isArray(n);
export const isObject = obj => _.isObject(obj);

export const toString = s => (isString(s) ? s : s.toString());
