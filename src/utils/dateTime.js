// @flow
import dateFns, { isDate, format as f } from 'date-fns';
import daLocale from 'date-fns/locale/da';
import enLocale from 'date-fns/locale/en';

import { isString, isObject } from './common';
import { type DateTimeProps } from '../components/elements/DateTime';

export const toDate = (date: Date | string) => {
  if (!date) return new Date();
  if (isDate(date)) return date;
  if (isString(date)) return toDate(new Date(date));

  console.warn('[newDate]: could not create date object from:', date);
  return new Date();
};

export const now = (date: Date | string) => toDate(date);

/**
 * Formats a date or time to a standardized format
 * @param {*} param0
 */
export const format = ({ value, customFormat, compact = false, mode = 'dateTime' }: DateTimeProps) => {
  const getDisplayFormat = m => {
    if (customFormat && isString(customFormat)) return customFormat;

    switch (m) {
      case 'date':
        if (compact) return 'MM-D-YY';
        return 'MMMM Mo YYYY';
      case 'time':
        return 'HH:mm a';
      case 'dateTime':
        return `${getDisplayFormat('date')}, ${getDisplayFormat('time')}`;
      default:
        return '';
    }
  };

  return f(toDate(value), getDisplayFormat(mode), { locale: enLocale });
};
