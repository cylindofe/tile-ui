import { observable, action, computed } from 'mobx';
import NotificationStore from './stores/notificationStore';
import ModalStore from './stores/modalStore';
import InfoBarStore from './stores/infoBarStore';
import UIStore from './stores/uiStore';

/**
 * Proxy API to update stores
 */
export default class Tile {
  @observable booted: boolean = false;

  constructor() {
    /**
     * New store instances
     */
    this.notificationStore = new NotificationStore();
    this.modalStore = new ModalStore();
    this.uiStore = new UIStore();
    this.infoBarStore = new InfoBarStore();

    /**
     * Public store methods
     * * All methods added here will be available through Tile.methodName()
     */

    /* Modal methods */
    this.showModal = this.modalStore.showModal;
    /* Notification methods */
    this.showNotification = this.notificationStore.showNotification;
    /* UI methods */
    this.setLoading = this.uiStore.setLoading;
    /* InfoBar methods */
    this.showInfoBar = this.infoBarStore.showInfoBar;
  }

  /**
   *  Boots Tile with custom config.
   *  @param config : {theme: {}, rebootOnLoad: boolean}
   */
  @action
  boot = (config = {}) => {
    const { theme, rebootOnLoad } = config;

    this.uiStore.setTheme(theme, rebootOnLoad);
    this.booted = true;
  };
}
