const path = require('path');

const rootDir = path.dirname(__dirname);

module.exports = {
  globals: {
    NODE_ENV: 'test',
  },
  verbose: true,
  testMatch: ['**/?(*.)test.js?(x)'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
  setupFiles: ['<rootDir>/test-shim.js', '<rootDir>/test-setup.js'],
  moduleDirectories: ['node_modules', 'src'],
  transformIgnorePatterns: ['/node_modules/(?!lodash-es)'],
  modulePaths: ['<rootDir>/src', '<rootDir>/node_modules'],
};
