// @flow
import React from 'react';
import styled from 'styled-components';
import { H2, P, Separator } from '../../src';

const Section = styled.section`
  margin: 80px 0px;
`;

type Props = {
  section: Function,
  title: string,
  description: string,
};

export const StorySection = ({ section, title, description }: Props) => (
  <Section>
    <H2>{title}</H2>
    <P marginBottom={10}>{description}</P>
    {section()}
    <Separator />
  </Section>
);
