import React from 'react';
import { observer } from 'mobx-react';

import TILE, { lodash as _, AppContainer, H1, H2, Spacer, P, Row, Col, Box, Separator, Container } from '../../src';

@observer
export class BreakPointDisplay extends React.Component {
  constructor(props) {
    super(props);
    this.uiStore = TILE.uiStore;
  }

  render() {
    const { breakpointMinWidth, theme, windowInnerWidth } = this.uiStore;

    return (
      <Box borderRadius={1} marginTop={8} elevation={4} padding={4}>
        <P fontWeight="bold">Breakpoints</P>
        <Row>
          <P marginBottom={0}>window width: {windowInnerWidth}px</P>
        </Row>
        <Separator />
        <Row>
          <P>breakpoint min: {theme.breakpoint[breakpointMinWidth]}px</P>
        </Row>
        <Row>
          <P marginBottom={0}>breakpoint max: {theme.breakpoint[breakpointMinWidth + 1]}px</P>
        </Row>
      </Box>
    );
  }
}
