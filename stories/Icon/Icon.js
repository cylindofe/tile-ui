import React from 'react';
import { isArray } from 'lodash-es';
import styled, { withTheme } from 'styled-components';
import { lighten, darken, transparentize } from 'polished';
import { lodash as _, AppContainer, H1, H2, Spacer, P, Row, Col, Box, Separator, Container } from '../../src';
import { StorySection } from '../components/StorySection';
import { IconList } from './IconList';

const SimpleGrid = () => <Row />;

const sections = [
  {
    title: 'List of all icons',
    description: 'See all icons in TILE',
    section: IconList,
  },
];

export const Icon = ({ config }) => (
  <ThemeWrapper>
    <H1>Icon</H1>
    {_.map(sections, (section, i) => <StorySection key={i} {...section} />)}
  </ThemeWrapper>
);

const ThemeWrapper = styled.div`
  padding: 80px;
`;
