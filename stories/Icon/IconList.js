import React from 'react';
import { isArray } from 'lodash-es';
import styled, { withTheme } from 'styled-components';
import { lighten, darken, transparentize } from 'polished';
import {
  lodash as _,
  AppContainer,
  Icon,
  H1,
  H2,
  Spacer,
  P,
  Row,
  Col,
  Box,
  Separator,
  Container,
  HoverTransition,
} from '../../src';
import { StorySection } from '../components/StorySection';
import Icons from '../../src/components/elements/Icon/svgPaths';

export const IconList = () => {
  const icons = _.map(Icons, (key, val) => val);

  return (
    <Box>
      <Row>
        {_.map(icons, i => (
          <Box width={[1, 1 / 2, 1 / 4]} key={i}>
            <HoverTransition transition="scale">
              <Box borderRadius={1} paddingTop={8} paddingBottom={8} padding={4} elevation={4} margin={4}>
                <Row marginBottom={8} justifyContent="center">
                  <Icon size={30} icon={i} />
                </Row>
                <Row justifyContent="center">
                  <P fontWeight="bold">"{i}"</P>
                </Row>
              </Box>
            </HoverTransition>
          </Box>
        ))}
      </Row>
    </Box>
  );
};
