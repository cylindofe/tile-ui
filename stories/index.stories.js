import React from 'react';
import { storiesOf, addDecorator } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { Welcome } from '@storybook/react/demo';
import { withInfo } from '@storybook/addon-info';
import { AppContainer, Button, theme, lodash as _ } from '../src';
import { PreviewContainer } from './components';
import { Theme } from './Theme';
import { Grid } from './Grid';
import { Icon } from './Icon';
import { Form } from './Form';

const appContainerConfig = {
  bootInternally: true,
  modalContainer: false,
  loadingContainer: false,
  notificationContainer: false,
};

const alternativeThemeConfig = {
  bootInternally: true,
  modalContainer: true,
  loadingContainer: true,
  notificationContainer: true,
  rebootOnLoad: true,
  theme: {
    themeValues: _.extend({}, theme.defaultTheme.themeValues, {
      color: _.extend({}, theme.defaultTheme.themeValues.color, { primary: '#AE48C6' }),
    }),
    componentDefaults: theme.defaultTheme.componentDefaults,
  },
};

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

/**
 * Theme
 */
storiesOf('Theme', module)
  .add('Default Theme', () => <Theme />)
  .add('Alternative Theme', () => <Theme config={alternativeThemeConfig} />);

/**
 * Grid
 */
storiesOf('Grid', module)
  .addDecorator(getStory => <AppContainer {...appContainerConfig}>{getStory()}</AppContainer>)
  .add('Grid overview', () => <Grid />);

/**
 * Button
 */
storiesOf('Button', module)
  .addDecorator(getStory => <AppContainer {...appContainerConfig}>{getStory()}</AppContainer>)
  .add('Default prop', withInfo(`hello`)(() => <Button onClick={action('clicked')}>Hello Button</Button>))
  .add(
    'Danger button',
    withInfo(`hello`)(() => (
      <Button color="danger" onClick={action('clicked')}>
        Hello Button
      </Button>
    ))
  )
  .add(
    'With icon',
    withInfo(`hello`)(() => (
      <Button icon="emoticon" onClick={action('clicked')}>
        Hello Button
      </Button>
    ))
  )
  .add('Only icon', withInfo(`hello`)(() => <Button icon="emoticon" onClick={action('clicked')} />))
  .add(
    'Round button',
    withInfo(`hello`)(() => (
      <Button shape="round" onClick={action('clicked')}>
        I'm Round
      </Button>
    ))
  );

storiesOf('Icon', module)
  .addDecorator(getStory => <AppContainer {...appContainerConfig}>{getStory()}</AppContainer>)
  .add('Simple icon', withInfo()(() => <Icon />));

storiesOf('Form', module)
  .addDecorator(getStory => <PreviewContainer config={appContainerConfig}>{getStory()}</PreviewContainer>)
  .add('Overview', withInfo()(() => <Form />));
