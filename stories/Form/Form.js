import React from 'react';
import { observer } from 'mobx-react';
import { withFormik } from 'formik';

import TILE, {
  utils,
  lodash as _,
  AppContainer,
  H1,
  H2,
  Spacer,
  P,
  Row,
  Col,
  Box,
  Separator,
  Container,
  Input,
  Button,
  FormElement,
  Form,
} from '../../src';
import { StorySection } from '../components/StorySection';
import { onFormChange, getFormValues } from '../../src/utils';

export class FormExample extends React.Component {
  selectOptions = [
    { value: 'Food', label: 'Food', id: 'food' },
    { value: 'Being Fabulous', label: 'Being Fabulous', id: 'being' },
    { value: 'Ken Wheeler', label: 'Ken Wheeler', id: 'ken' },
    { value: 'ReasonML', label: 'ReasonML', id: 'reasonml' },
    { value: 'Unicorns', label: 'Unicorns', id: 'unicorns' },
    { value: 'Kittens', label: 'Kittens', id: 'kittens' },
  ];

  onSubmit = values => {
    setTimeout(() => {
      alert(JSON.stringify(values, null, 2));
    }, 1000);
  };

  render() {
    console.log('## utils ## ', utils);

    return (
      <React.Fragment>
        <StorySection
          title="Input"
          description="TILE form elements are simple view components designed to work with Formik, a form library that removes the main pains when dealing with forms in react."
          section={() => <SimpleForm options={this.selectOptions} onSubmit={this.onSubmit} />}
        />
      </React.Fragment>
    );
  }
}

const SimpleFormInner = ({
  values,
  touched,
  errors,
  dirty,
  isSubmitting,
  handleChange,
  setFieldValue,
  handleBlur,
  handleSubmit,
  handleReset,
  ...rest
}) => {
  const { options } = rest;

  return (
    <Form isSubmitting={isSubmitting} onSubmit={handleSubmit}>
      <FormElement
        label="Email"
        id="email"
        placeholder="Enter your email"
        type="input"
        required={false}
        value={values.email}
        onChange={handleChange}
        onBlur={handleBlur}
        touched={touched.email}
        error={errors.email}
      />

      <FormElement
        options={options}
        onChange={setFieldValue}
        onBlur={handleBlur}
        id="topic"
        label="Topics"
        type="select"
        value={values.topic}
        lines={5}
      />
    </Form>
  );
};

const SimpleForm = withFormik({
  mapPropsToValues: () => ({ email: '', topic: { value: 'Kittens', label: 'Kittens' } }),
  validate: (values, props) => {
    const errors = {};
    if (values.email === 'lars') errors.email = 'oh nooo!';
    return errors;
  },
  handleSubmit: (values, { setSubmitting, props }) => {
    const { onSubmit } = props;
    setSubmitting(false);
    onSubmit(values);
  },
})(SimpleFormInner);
